## v0.4.2 (February 24, 2020)

### Admin Changes

* When using the supported RPM, `jacamar-auth` application is now deployed
  to `/opt/jacamar/bin` to best support `CAP_SETUID`/`CAP_SETGID`
  workflows
  ([!97](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/97))
* Configurable `root_dir_creation = true` in Jacamar-Auth supports
  creation of top level user owned directory by a privileged user
  ([!99](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/99)).
* Support for Runner versions 13.8 and removal of federation patch
  ([!96](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/96)).

### Bug & Development Fixes

* Target server host for script augmentation of target URLs
  ([!98](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/98)).
* Improved static analysis and linting during CI process
  ([!95](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/95),
  [!94](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/94)).

## v0.4.1 (February 16, 2020)

### General Changes

* Added *CheckJWT* to the *rules* package
  ([!91](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/91)).

### Admin Changes

* Support new enhanced broker registration requirements
  ([!93](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/93)).
* Update GitLab-Runner to version `13.8.0`, with minimized
  patching requirements for testing purposes
  ([!90](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/90)).

### Bug & Development Fixes

* Correctly remove `CI_JOB_JWT` from user environment only when federation or
  broker service is enabled
  ([!89](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/89)).

## v0.4.0 (January 25, 2020)

### General Changes

* Migrated two internal packages to now support a public API for key
  functionality used across multiple EPC CI projects
  ([!58](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/58)).
* Addresses potential case in which failed Cobalt/Slurm jobs did
  not observe a defined NFS timeout/delay
  ([!81](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/81)).

### Admin Changes

* The cleanup stage (`clean` subcommand) will now require the configuration_
  file be provided with the `--configuration` argument
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Integrated all Federation information/context into the RunAs validation
  scripts, removing Federation only scripting requirements
  ([!65](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/65),
  [!86](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/86)).
* Pre-RunAs list (allow/block) validation can now optionally be enabled via the
  configuration ([!68](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/68)).
* Jacamar subcommand (`jacamar translate`) added to translate a runner
  configuration from the forked deployment to a currently observed
  Jacamar configuration
  ([!63](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/63)).
* Added experimental plugin support for RunAs validation
  ([!76](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/76)).
* `jacamar--auth` now supports an `--unobfuscated` flag that
  allows all errors to appear regardless of stage.
  ([!83](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/83))
* Update supported GitLab-Runner to version `13.7.0`, with minimized
  patching requirements
  ([!72](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/72)).
* Added configurable timeout for Jacamar-Auth to wait before sending `SIGKILL`
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Updated OLCF focused testing structure for Ascent
  ([!62](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/61)).

### Bug & Development Fixes

* Updated to Go release 1.15.6
  ([!64](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/64)).
* Correctly establish default values for configuration_ in all cases
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71)).
* Cleaner error handling within `jacamar-auth` to enforce default obfuscation
  for any potentially sensitive error
  ([!74](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/74)).
* Correctly leverage `_prefix` macro in RPM spec
  ([!79](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/79)).
* Added Go/GCC to Pavilion containers and updated associated test
  scripts to build Jacamar requirements
  ([!78](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/78),
  [!80](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/80)).
* System logging of `jacamar-auth` now handled through the Logrus
  packages ([!82](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/82)).
* Improve RPM related make commands
  ([!85](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/85)).

## v0.3.2 (December 11, 2020)

### Bug & Development Fixes

* Correctly monitor and wait on background child processes to
  complete that have been initiated by a user’s script
  ([!59](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/59)).
* Use Go’s `net/url` package when identifying target host for broker
  interactions.
