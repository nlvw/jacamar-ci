package jacamartst

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func AssertNoError(t *testing.T, err error) {
	assert.NoError(t, err)
}

func AssertError(t *testing.T, err error) {
	assert.Error(t, err)
}
