// Package jacamartst maintains shared test functionality.
package jacamartst

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// SetEnv sets the OS environment variables.
func SetEnv(m map[string]string) {
	for key, value := range m {
		_ = os.Setenv(key, value)
	}
}

// UnsetEnv removes the OS environment variables.
func UnsetEnv(m map[string]string) {
	for key := range m {
		_ = os.Unsetenv(key)
	}
}

// WriteTmpFile creates a temporary file with the provided prefix
// and the contents. Full file path returned if successful.
func WriteTmpFile(t *testing.T, prefix, contents string) string {
	file, _ := ioutil.TempFile(t.TempDir(), prefix)

	text := []byte(contents)
	_, err := file.Write(text)
	assert.NoError(t, err, "error writing contents")

	file.Close()
	_ = os.Chmod(file.Name(), 0700)

	return file.Name()
}

// NewURL implements the Mockgen Matcher interface  to compare a URL string to a URL found in
// a http.request structure.
func NewURL(u string) urlMatcher {
	y, _ := url.Parse(u)
	return urlMatcher{y: y}
}

type urlMatcher struct {
	y *url.URL
}

func (u urlMatcher) Matches(x interface{}) bool {
	return *u.y == *x.(*http.Request).URL
}

func (u urlMatcher) String() string {
	return fmt.Sprintf("is equal to %v", u.y)
}

// ClosingBuffer implements an io.ReaderCloser that
// always returns nil.
type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() error {
	return nil
}
