%{!?_version: %define _version 0.4.0 }

Name:           jacamar-ci
Version:        %{_version}
Release:        1%{?dist}
Summary:        HPC focused CI/CD driver for the GitLab custom executor model.
Vendor:         Exascale Computing Project - 2.4.4
Packager:       ECP CI Infrastructure <ecp-ci-infrastructure@elist.ornl.gov>

# License details: https://gitlab.com/ecp-ci/jacamar-ci/-/blob/develop/LICENSE
License:        MIT/Apache 2.0
URL:            https://gitlab.com/ecp-ci/%{name}
Source0:        https://gitlab.com/ecp-ci/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

# Go is required for build but a newer version (see go.mod)
# than is available on most distributions.
BuildRequires:  bash
BuildRequires:  git
BuildRequires:  glibc
BuildRequires:  make

Requires:       bash
Requires:       gitlab-runner-ecp

%global jac /opt/jacamar

%description
Jacamar CI is an open source project designed to provide a bridge
between traditional GitLab CI pipelines and HPC resources. All this
is accomplished by first conforming to the supported custom executor
model for executing jobs. Beyond this, Jacamar, manages additional
responsibilities; user authorization, downscoping mechanisms,
scheduler interactions, and a robust set of administrative
configuration options.

%prep
%if %{defined _sha256sum}
echo "%{_sha256sum}  %SOURCE0" | sha256sum -c -
%endif
%setup -q -n %{name}-v%{version}

%build

make build VERSION=%{version}

%install

mkdir -p %{buildroot}%{jac}/bin
mkdir -p %{buildroot}%{_bindir}

install -m 700 binaries/jacamar-auth %{buildroot}%{jac}/bin
install -m 755 binaries/jacamar %{buildroot}%{_bindir}

%files

%attr(0700, root, root) %{jac}
%{_bindir}/jacamar

