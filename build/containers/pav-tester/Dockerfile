FROM python:3-slim

RUN adduser -q user \
    && adduser -q teser \
    && adduser -q passtest \
    && adduser -q overuser \
    && adduser -q newuser

ARG PAV_SHA="dde1d79072ef8580ec571af4eadb6e6e8009be36"

RUN apt-get update \
    && apt-get install -qy \
        gcc \
        git \
        make \
        procps \
        rsyslog \
        wget \
    && touch /var/log/syslog \
    && git clone -q https://github.com/hpc/pavilion2.git \
    && cd pavilion2 \
        && git checkout ${PAV_SHA} \
        && git submodule update --init --recursive \
        && pip -q install -r requirements.txt \
        && mv bin/* /usr/local/bin/ \
        && mv lib/* /usr/local/lib/ \
    && cd .. \
    && rm -rf pavilion2 \
    && bash -c pav --help

RUN pip install pyyaml flask

# https://golang.org/dl/
ARG GO_VERSION="1.15.6"
ARG GO_ARCH="amd64"
ARG GO_SHA="3918e6cc85e7eaaa6f859f1bdbaac772e7a825b0eb423c63d3ae68b21f84b844"
ARG GO_URL="https://golang.org/dl/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz"

ENV GOPATH /go

RUN wget -q -O go.tgz "${GO_URL}" && \
	echo "${GO_SHA} *go.tgz" | sha256sum -c - && \
	tar -C /usr/local -xzf go.tgz && \
	rm go.tgz && \
	mkdir -p "$GOPATH/src" "$GOPATH/bin" && \
	chmod -R 777 "$GOPATH"

ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
