#!/bin/bash

token=${FEDERATED_AUTH_TOKEN}
user=${FEDERATED_USERNAME}

if [ "${1}" == "passtest" ] ; then
  if [ "${RUNAS_CURRENT_USER}" == "passtest" ] ; then
    exit 0
  else
    echo "Invalid RUNAS_CURRENT_USER"
    exit 1
  fi
fi

if [ "$user" == "fedUser" ] ; then
	if [ "$token" == "abc123" ] ; then
		echo '{"username": "user"}'
		exit 0
	elif [ "$token" == "jsonError" ]; then
		echo '"username": 123}'
		exit 0
	elif [ "$token" == "override" ]; then
		echo '{"username": "overuser"}'
		exit 0
	fi
	exit 1
elif [ "$user" == "envchecker" ] ; then
  [ -z "$FEDERATED_AUTH_TOKEN" ] && exit 1
  [ -z "$FEDERATED_USERNAME" ] && exit 1
  echo '{"username": "user"}'
  exit 0
fi

exit 1
