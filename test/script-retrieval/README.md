# Script Retrieval

Use the supplied `run.bash` as the `run_exec` script for
a custom executor, coupled with the provided `gitlab-ci.yml`
file. This will create a clean version of each execution
script under `/var/tmp/<runnerVersion>`.
