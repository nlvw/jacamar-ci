#!/usr/bin/env bash

# Execute Pavilion tests within Docker container to test Jacamar's Slurm
# executor. This method closely mirrors the existing docker.bash script;
# however, may not present a long terms solution if a sufficient facility
# environment to test Slurm is identified.

set -eo pipefail
set +o noclobber

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)
export PAV_CONFIG_DIR="${ROOT_DIR}/test/pavilion/docker"

ci_dir="/builds/ecp-ci/jacamar-ci"

sacctmgr_update() {
  sleep 10 # wait for database startup

  sacctmgr -i create cluster linux
  # ignore error adding user
  sacctmgr -i create user user name=user cluster=linux || echo "Added user"

  slurmctld restart
}

test() {
  # Ensure downscope user has access to view/create test directories.
  chmod -R 755 "${ROOT_DIR}/test/scripts"
  chmod 777 "${ROOT_DIR}/test/pavilion/docker/working_dir"

  pav_image="registry.gitlab.com/ecp-ci/jacamar-ci/pav-tester:latest"
  docker pull ${pav_image}

  slurm_image="registry.gitlab.com/ecp-ci/jacamar-ci/slurm-tester:latest"
  docker pull ${slurm_image}

  build_image="registry.gitlab.com/ecp-ci/jacamar-ci/centos7-builder:latest"
  docker pull ${build_image}

  echo "Building Jacamar Binaries..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -w ${ci_dir} \
    -t ${build_image} \
    bash -c "make build"

  echo "Testing Jacamar + Slurm Executor..."
  docker run \
    -h ernie \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t "${slurm_image}" \
    bash -c "${ci_dir}/test/scripts/pav/slurm.bash sacctmgr \
            && su - user -c 'ROOT_DIR=${ci_dir} ${ci_dir}/test/scripts/pav/slurm.bash user'"

  echo "Review Results..."
  docker run \
    -v "${ROOT_DIR}:${ci_dir}" \
    -v "${ROOT_DIR}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -t ${pav_image} \
    bash -c "cd ${ci_dir}/test/pavilion/docker \
            && pav status --all --limit 1000 \
            && pav status -a -l 1000 | if grep -q FAIL; then exit 1; fi"
}

run_user() {
  export CI_PROJECT_DIR=${ci_dir}
  python ${ci_dir}/test/pavilion/shared/sequence.py \
            ${ci_dir}/test/pavilion/docker/tests/slurm.yaml \
            ${ci_dir}/test/pavilion/docker
}

case "${1}" in
  sacctmgr)
    sacctmgr_update
    ;;
  test)
    test
    ;;
  user)
    run_user
    ;;
  *)
    echo "Invalid argument: ${0} (sacctmgr|test|user)"
    exit 1
    ;;
esac
