#!/usr/bin/env bash

# Test locally maintained Spack packages as part of a CI job.

set -eo pipefail
set -o xtrace 
 
git clone https://github.com/spack/spack --branch develop --single-branch spack

pushd spack
  git checkout ${SPACK_COMMIT}
popd

source spack/share/spack/setup-env.sh

pushd ${CI_PROJECT_DIR}/test/spack/gitlab
    spack env activate .
    spack external find git go tar
    spack install jacamar-ci ^gitlab-runner+jacamar
    spack test run
    spack test results -l
popd
