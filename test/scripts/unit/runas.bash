#!/bin/bash

# Used in validation package tests.
# Note, checksum validated, verify before updating.

if [ "$#" -eq 1 ]; then
	user="$1"

	[ ! -z "${FEDERATED_USERNAME}" ] && user=${FEDERATED_USERNAME}

	if [ "$user" == "gitlab" ] ; then
		echo '{"username": "new"}'
		exit 0
	elif [ "$user" == "json" ] ; then
		echo '"username": 123}'
		exit 0
	elif [ "$user" == "shared" ] ; then
		echo '{"username": "new", "shared": "test"}'
		exit 0
	elif [ "$user" == "none" ] ; then
		exit 0
	fi
elif [ "$#" -eq 2 ]; then
	user="$2"
	service_account="$1"
else
    exit 1
fi

if [ "$service_account" == "pass" ] ; then
	exit 0
fi

exit 1
