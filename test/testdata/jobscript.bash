#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running job script..."\n'
exit 0
