package bash

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

type shell struct {
	abs            *command.AbstractCommander
	cmd            *exec.Cmd
	etcProfileOnly bool
	noBashProfile  bool
}

func (s *shell) PipeOutput(stdin string) error {
	if s.cmd == nil {
		return errors.New("command (exec.Cmd) not found, verify properly built")
	}

	cmd := &exec.Cmd{}
	s.abs.CloneCmd(s.cmd, cmd)

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	cmd.Stdin = bytes.NewBufferString(s.prepStdin(stdin))

	return s.abs.RunCmd(cmd)
}

func (s *shell) ReturnOutput(stdin string) (string, error) {
	if s.cmd == nil {
		return "", errors.New("command (exec.Cmd) not found, verify properly built")
	}

	cmd := &exec.Cmd{}
	s.abs.CloneCmd(s.cmd, cmd)

	b := bytes.Buffer{}
	cmd.Stdout = &b
	cmd.Stderr = &b
	cmd.Stdin = bytes.NewBufferString(s.prepStdin(stdin))

	err := s.abs.RunCmd(cmd)

	return b.String(), err
}

func (s *shell) prepStdin(stdin string) string {
	var sb strings.Builder
	if s.etcProfileOnly {
		sb.WriteString("source /etc/profile\n")
	}
	sb.WriteString(stdin)
	return sb.String()
}

func (s *shell) CommandDir(dir string) {
	s.cmd.Dir = dir
}

func (s *shell) SigtermReceived() bool {
	return s.abs.TermCaptured
}

func (s *shell) AppendEnv(e []string) {
	s.cmd.Env = append(s.cmd.Env, e...)
}

func (s *shell) build(name string, arg ...string) {
	/* #nosec */
	// launching subprocess with variables required
	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	s.cmd = cmd
}

// Factory is used to defined requirements and interface to all related methods.
type Factory struct {
	AbsCmdr *command.AbstractCommander
}

// CreateStdShell generates a shell that is targeted at authorization level command execution.
// Upon successful creation a command ($ bash) will be defined for future runs, via
// stdin. No setuid (or similar) mechanism are enabled and the current environment is
// replicated in the command. It is expected that this is used as part of a Jacamar-Auth
// application with no downscoping specified.
func (f Factory) CreateStdShell() (*shell, error) {
	if f.AbsCmdr == nil {
		return nil, errors.New(
			"AbstractCommander structure is nil, verify factory for CreateStdShell",
		)
	}

	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
	}

	s.build("bash")

	return s, nil
}

// CreateBaseShell generates a shell that is targeted at job level command/script execution.
// Upon successful creation a command ($ env -i HOME=? bash ?) will be defined for future
// runs, via stdin. Runs are always presented with a completely clean bash login shell.
// It is expected that this is invoked as part of the Jacamar application.
func (f Factory) CreateBaseShell(au authuser.Authorized) (*shell, error) {
	if f.AbsCmdr == nil {
		return nil, errors.New(
			"AbstractCommander structure is nil, verify factory for CreateBaseShell",
		)
	}

	s := &shell{
		abs:            f.AbsCmdr,
		cmd:            nil,
		etcProfileOnly: envparser.EtcProfileOnly(),
		noBashProfile:  envparser.NoBashProfile(),
	}

	// Override and enforce default behaviors.
	s.abs.KillTimeout = 0 * time.Second

	arg := []string{"-i", fmt.Sprintf("HOME=%s", au.HomeDir()), "bash"}

	if s.etcProfileOnly || s.noBashProfile {
		arg = append(arg, "--noprofile")
	} else { // Default login shell.
		arg = append(arg, "--login")
	}

	s.build("/usr/bin/env", arg...)

	return s, nil
}

// CreateSetuidShell generates a setuid enabled shell targeting the authorized user.
// Upon successful creation a command ($ bash) will be defined for future
// runs, via stdin. Runs are always presented with a controlled command environment. Only
// custom executor defined variables (e.g. CUSTOM_ENV_) will be replicated. It is
// expected that this is used as part of a Jacamar-Auth application when downscoping is specified.
func (f Factory) CreateSetuidShell(
	au authuser.Authorized,
	broker bool,
	state envparser.StatefulEnv,
) (*shell, error) {
	if f.AbsCmdr == nil {
		return nil, errors.New(
			"AbstractCommander structure is nil, verify factory for CreateSetuidShell",
		)
	}

	s := &shell{
		abs: f.AbsCmdr,
		cmd: nil,
	}

	// This lookup is occurring as privileged user (root) in their environment.
	s.build("bash")
	if err := s.targetValidUser(au, broker, state); err != nil {
		return nil, fmt.Errorf("unable to generate setuid command: %w", err)
	}
	s.CommandDir(au.HomeDir())

	return s, nil
}
