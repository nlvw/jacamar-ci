package command

import (
	"os/exec"
	"reflect"
	"syscall"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestNewAbsCmdr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(gomock.Eq(
		"Invalid time duration in configuration (KillTimeout), defaults to 30s.",
	))

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{})

	t.Run("enforce new abstract commander default behavior", func(t *testing.T) {
		done := make(chan struct{})
		go func() {
			// We can just close, we aren't testing this here.
			time.Sleep(1 * time.Second)
			close(done)
		}()
		a := NewAbsCmdr(cfg.General(), m, done)

		assert.False(t, a.NotifyTerm, "NotifyTerm expected default is false")
		assert.False(t, a.TermCaptured, "TermCaptured expected default is false")

		a.EnableNotifyTerm()
		assert.True(t, a.NotifyTerm, "NotifyTerm expected to be changed")
	})
}

func TestCloneCmd(t *testing.T) {
	cmd := exec.Command("bash", "test")
	cmd.Env = append(cmd.Env, "foo=bar")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid: 1,
		Gid: 1,
	}

	newCmd := &exec.Cmd{}

	tests := []struct {
		name string
		src  *exec.Cmd
		dest *exec.Cmd
	}{
		{
			name: "nil src pointer provided",
			src:  nil,
			dest: newCmd,
		}, {
			name: "copying standard command",
			src:  cmd,
			dest: newCmd,
		}, {
			name: "nil dest pointer provided",
			src:  cmd,
			dest: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AbstractCommander{}
			a.CloneCmd(tt.src, tt.dest)

			if tt.dest != nil && tt.src != nil {
				assert.True(t, reflect.DeepEqual(tt.src, tt.dest))
			}
		})
	}
}

func TestAbstractCommander_RunCmd(t *testing.T) {
	tests := map[string]signalTests{
		"run successful command": {
			a:   &AbstractCommander{},
			cmd: exec.Command("date"),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"run unsuccessful command": {
			a:   &AbstractCommander{},
			cmd: exec.Command("exit", "1"),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.a.RunCmd(tt.cmd)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
