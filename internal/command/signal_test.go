package command

import (
	"log"
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type signalTests struct {
	a   *AbstractCommander
	cmd *exec.Cmd

	mockInteraction func(*exec.Cmd, chan struct{}, *AbstractCommander)

	assertAbsMech func(*testing.T, *AbstractCommander)
	assertCommand func(*testing.T, *exec.Cmd)
	assertError   func(*testing.T, error)
}

func TestAbstractCommander_MonitorSignal(t *testing.T) {
	tests := map[string]signalTests{
		"completed (done) without signal received": {
			a: &AbstractCommander{},
			mockInteraction: func(cmd *exec.Cmd, done chan struct{}, a *AbstractCommander) {
				time.Sleep(3 * time.Second)
				close(done)
			},
			assertAbsMech: func(t *testing.T, a *AbstractCommander) {
				assert.False(t, a.TermCaptured)
			},
		},
		"signal identified, TermCaptured (use notifyProcess)": {
			a: &AbstractCommander{},
			mockInteraction: func(cmd *exec.Cmd, done chan struct{}, a *AbstractCommander) {
				time.Sleep(3 * time.Second)
				proc := &os.Process{
					Pid: os.Getpid(),
				}
				notifyProcess(proc)
			},
			assertAbsMech: func(t *testing.T, a *AbstractCommander) {
				assert.True(t, a.TermCaptured)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			done := make(chan struct{})
			if tt.mockInteraction != nil {
				go tt.mockInteraction(tt.cmd, done, tt.a)
			}

			tt.a.MonitorSignal(done)

			if tt.assertAbsMech != nil {
				tt.assertAbsMech(t, tt.a)
			}
		})
	}
}

func TestAbstractCommander_MonitorTermination(t *testing.T) {
	tests := map[string]signalTests{
		"nil command argument provided": {
			a:   &AbstractCommander{},
			cmd: nil,
		},
		"nil process argument provided": {
			a: &AbstractCommander{},
			cmd: &exec.Cmd{
				Process: nil,
			},
			mockInteraction: func(cmd *exec.Cmd, done chan struct{}, a *AbstractCommander) {
				// need to close to stop monitoring
				a.TermCaptured = true
			},
		},
		"completed (done) without signal received": {
			a:   &AbstractCommander{},
			cmd: &exec.Cmd{},
			mockInteraction: func(cmd *exec.Cmd, done chan struct{}, a *AbstractCommander) {
				time.Sleep(3 * time.Second)
				close(done)
			},
		},
		"terminate command, short notification window": {
			a: &AbstractCommander{
				NotifyTerm:  true,
				KillTimeout: 1 * time.Second,
			},
			cmd: exec.Command("sleep", "30"),
			mockInteraction: func(cmd *exec.Cmd, done chan struct{}, a *AbstractCommander) {
				err := cmd.Run()
				if err != nil {
					log.Fatal(err)
				}
				a.TermCaptured = true
				_ = cmd.Wait()
			},
			assertCommand: func(t *testing.T, cmd *exec.Cmd) {
				assert.True(t, cmd.ProcessState.Exited())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			done := make(chan struct{})
			if tt.mockInteraction != nil {
				go tt.mockInteraction(tt.cmd, done, tt.a)
			}

			tt.a.MonitorTermination(tt.cmd, done)

			if tt.assertCommand != nil {
				tt.assertCommand(t, tt.cmd)
			}
		})
	}
}
