package authuser

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

type dirTest struct {
	buildEnv string
	gen      configure.General
	req      envparser.RequiredEnv
	user     *UserContext
	claims   gitlabjwt.Claims

	assertError       func(*testing.T, error)
	assertUserContext func(*testing.T, *UserContext)
}

func TestUserContext_identifyDirectories(t *testing.T) {
	testContext := &UserContext{
		username: "user",
		homeDir:  "/home/user",
	}

	tests := map[string]dirTest{
		"undefined username in UserContext": {
			user: &UserContext{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected, aborting build directory identification",
				)
			},
		},
		"undefined homedir in UserContext": {
			user: &UserContext{
				username: "user",
				homeDir:  "",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected, aborting build directory identification",
				)
			},
		},
		"DataDir defined for user's home, no custom builds": {
			user: testContext,
			gen: configure.General{
				DataDir: "$HOME",
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.baseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/builds/short/000", u.buildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/cache", u.cacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/scripts/short/0/group/project/123", u.scriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for user's home, custom builds": {
			user: testContext,
			gen: configure.General{
				DataDir:        "$HOME",
				CustomBuildDir: true,
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.buildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"DataDir defined for target directory, no custom builds": {
			user: testContext,
			gen: configure.General{
				DataDir: "/ci",
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/ci/user", u.baseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.buildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.cacheDir, "UserContext - cacheDir")
				assert.Equal(
					t, "/ci/user/scripts/short/0/group/project/123", u.scriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for target directory, custom builds": {
			user: testContext,
			gen: configure.General{
				DataDir:        "/ci",
				CustomBuildDir: true,
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.buildsDir,
					"UserContext - buildsDir")
			},
		},
		"DataDir not defined in configuration": {
			user: testContext,
			gen:  configure.General{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"undefined 'data_dir' unsupported, please updated your configuration",
				)
			},
		},
		"Custom build directory enabled but not used": {
			user: testContext,
			gen: configure.General{
				DataDir:        "/ci",
				CustomBuildDir: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/ci/user/builds/short/000", u.buildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"Shared cache directory enabled and dataDir correctly defined": {
			user: testContext,
			gen: configure.General{
				DataDir: "/ci",
			},
			req:    testRedEnv,
			claims: testRedClaims,
			assertUserContext: func(t *testing.T, u *UserContext) {
				assert.Equal(
					t, "/ci/user", u.baseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.buildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.cacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/ci/user/scripts/short/0/group/project/123", u.scriptDir,
					"UserContext - scriptDir",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.buildEnv != "" {
				os.Setenv("CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR", tt.buildEnv)
				defer os.Unsetenv("CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR")
			}

			err := tt.user.identifyDirectories(tt.gen, tt.req, tt.claims)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, tt.user)
			}
		})
	}
}
