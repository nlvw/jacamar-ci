package validation

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
	jp "gitlab.com/ecp-ci/jacamar-plugins"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/rules"
)

const (
	// pluginSymName is the expected symbol name (pkg plugin).
	pluginSymName = "Validate"
)

type runAsCfg struct {
	file   string
	sha256 string
	// targetUser is a user proposed login (e.g. service account)
	targetUser string
	federated  bool
}

type runAsPlugin struct {
	runAsCfg
}

func (rp runAsPlugin) Execute(
	pay gitlabjwt.Claims,
	username string,
) (over jp.RunAsOverride, err error) {
	initializer, err := rp.runAsCfg.runAsPrep(pay, username)
	if err != nil {
		return
	}

	// we've verified file existence + checksum during preparations
	plugins, _ := filepath.Glob(rp.file)
	p, err := plugin.Open(plugins[0])
	if err != nil {
		return
	}

	symbol, err := p.Lookup(pluginSymName)
	if err != nil {
		return
	}

	validateFunc, ok := symbol.(func(jp.RunAsInit) (jp.RunAsOverride, error))
	if !ok {
		return over, errors.New("plugin has no 'func(jacamarplugins.RunAsInit)' function")
	}

	over, err = validateFunc(initializer)
	if err != nil {
		return
	}

	return finalizeOverride(initializer, over)
}

type runAsScript struct {
	runAsCfg
}

func (rs runAsScript) Execute(
	pay gitlabjwt.Claims,
	username string,
) (over jp.RunAsOverride, err error) {
	initializer, err := rs.runAsCfg.runAsPrep(pay, username)
	if err != nil {
		return over, err
	}

	ok, output := invokeRunAsScript(rs.file, initializer)
	if !ok {
		return over, fmt.Errorf("runas validation script failed: %s", output)
	}

	err = parseScriptStdout(output, &over)
	if err != nil {
		return over, err
	}

	return finalizeOverride(initializer, over)
}

func finalizeOverride(initializer jp.RunAsInit, over jp.RunAsOverride) (jp.RunAsOverride, error) {
	if over.Username == "" {
		if initializer.TargetUser != "" {
			over.Username = initializer.TargetUser
		} else {
			over.Username = initializer.CurrentUser
		}
	}

	if err := over.Validator(); err != nil {
		return jp.RunAsOverride{}, err
	}

	return over, nil
}

func (rc runAsCfg) runAsPrep(pay gitlabjwt.Claims, username string) (jp.RunAsInit, error) {
	if username == "" {
		return jp.RunAsInit{},
			errors.New("unexpected system error encountered, no current user can be identified")
	}

	err := fileStatus(rc.file, rc.sha256)
	return rc.create(pay, username), err
}

func (rc runAsCfg) create(pay gitlabjwt.Claims, username string) jp.RunAsInit {
	ra := jp.RunAsInit{
		TargetUser:  rc.targetUser,
		CurrentUser: username,
		JobJWT: jp.JobJWT{
			JobID:       pay.JobID,
			NamespaceID: pay.NamespaceID,
			PipelineID:  pay.PipelineID,
			ProjectID:   pay.ProjectID,
			ProjectPath: pay.ProjectPath,
			UserEmail:   pay.UserEmail,
			UserID:      pay.UserID,
			UserLogin:   pay.UserLogin,
		},
	}

	if rc.federated {
		// Only need to provide if federation is enabled/expected.
		ra.AuthToken = pay.AuthToken
		ra.FedUsername = pay.FedUserName
		if ra.FedUsername != "" {
			// If a federated username is provided it should be assumed
			// this is the current username (e.g. overwrite user login).
			ra.CurrentUser = ra.FedUsername
		}
	}

	return ra
}

// getRunAsUser attempts to find a user provided name in the custom executor
// provided environment. Nonexistent key/values will not results in an error.
func getRunAsUser(key string) (username string, err error) {
	if key != "" {
		username = strings.TrimSpace(os.Getenv(key))
	}

	v := validator.New()
	_ = v.RegisterValidation("username", rules.CheckUsername)
	err = v.Var(username, "username")

	return
}

// runasEnv builds environment to be append to the command for RunAs.
func runasEnv(initializer jp.RunAsInit) []string {
	return cmdEnv(
		reflect.TypeOf(initializer),
		reflect.ValueOf(&initializer).Elem(),
	)
}

// invokeRunAsScript executes the provided script passing it the verifier and username
// as arguments ($ script verifier username), returning if true (exit status = 0)
// and  false (!=0) along with any stdout.
func invokeRunAsScript(
	script string,
	initializer jp.RunAsInit,
) (bool, []byte) {
	var cmd *exec.Cmd
	/* #nosec */
	// launching subprocess with variables required
	if initializer.TargetUser != "" {
		cmd = exec.Command(script, initializer.TargetUser, initializer.CurrentUser)
	} else {
		cmd = exec.Command(script, initializer.CurrentUser)
	}

	// We want to avoid passing potential tokens to more process than necessary.
	tarEnv := runasEnv(initializer)
	for _, val := range os.Environ() {
		if !envparser.SupportedPrefix(val) {
			tarEnv = append(tarEnv, val)
		}
	}
	cmd.Env = tarEnv

	return runCommand(cmd)
}
