// +build linux,!cgo

package pwshell

import (
	"os/exec"
	"strings"
)

// FetchShell for Linux without CGO will leverage the getent(1)
// command to obtain a user's defined shell.
func FetchShell(uid string) (string, error) {
	cmd := exec.Command("getent", "passwd", uid)
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	s := strings.Split(strings.TrimSuffix(string(out), "\n"), ":")
	return s[6], nil
}
