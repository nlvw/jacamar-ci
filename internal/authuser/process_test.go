package authuser

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os/user"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-plugins"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_validation"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type processTests struct {
	auth     configure.Auth
	cfg      configure.Configurer
	claims   gitlabjwt.Claims
	env      envparser.ExecutorEnv
	valid    validators
	authFlow bool

	assertError      func(*testing.T, error)
	assertContext    func(*testing.T, UserContext)
	assertAuthorized func(*testing.T, Authorized)
}

func buildMockNoCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()
	m.EXPECT().General().Return(configure.General{}).AnyTimes()
	return m
}

func buildMockValidCurrentCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()
	m.EXPECT().General().Return(configure.General{
		DataDir: "/ci",
	}).AnyTimes()
	return m
}

func buildMockClaimsCurrent(ctrl *gomock.Controller) *mock_gitlabjwt.MockValidator {
	m := mock_gitlabjwt.NewMockValidator(ctrl)
	m.EXPECT().Parse().Return(gitlabjwt.Claims{
		UserLogin:   currentUsr.Username,
		ProjectPath: "group/project",
	}, nil).AnyTimes()
	return m
}

func buildMockClaimsRoot(ctrl *gomock.Controller) *mock_gitlabjwt.MockValidator {
	m := mock_gitlabjwt.NewMockValidator(ctrl)
	m.EXPECT().Parse().Return(gitlabjwt.Claims{
		UserLogin: "root",
	}, nil).AnyTimes()
	return m
}

func buildMockClaimsError(ctrl *gomock.Controller) *mock_gitlabjwt.MockValidator {
	m := mock_gitlabjwt.NewMockValidator(ctrl)
	m.EXPECT().Parse().Return(gitlabjwt.Claims{}, errors.New("error message")).AnyTimes()
	return m
}

func Test_processFlow(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]processTests{
		"error encountered while attempting to validate job jwt": {
			cfg: buildMockNoCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsError(ctrl),
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to parse supplied CI_JOB_JWT: error message")
			},
		},
		"authFlow disabled and no data_dir defined": {
			cfg: buildMockNoCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsCurrent(ctrl),
			},
			authFlow: false,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to identify target directories: undefined 'data_dir' unsupported, please updated your configuration")
			},
		},
		"attempt authorization as root while authFlow is enabled": {
			cfg: buildMockMinCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsRoot(ctrl),
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cannot execute as root while authorization is enforced")
			},
		},
		"attempt authorization as current user while authFlow is enabled": {
			cfg: buildMockMinCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsCurrent(ctrl),
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, fmt.Sprintf("cannot execute as %s while authorization is enforced", currentUsr.Username))
			},
		},
		"invalid authorization target, failed validateUser lists": {
			cfg: buildMockFullCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsRoot(ctrl),
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid authorization target user: a group allowlist exists, but root is not a member of any defined group")
			},
		},
		"valid current user": {
			cfg: buildMockValidCurrentCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsCurrent(ctrl),
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertContext: func(t *testing.T, u UserContext) {
				assert.Equal(t, currentUsr.Username, u.username, "target username")
				assert.Equal(t, currentUsr.Uid, u.uid, "target UID")
				assert.Equal(t, fmt.Sprintf("/ci/%s", u.username), u.baseDir, "target basedir")
			},
		},
		"invalid authorization target, failed validateUser lists (ListsPreValidation)": {
			cfg: buildMockPreListsCfg(ctrl),
			valid: validators{
				jobJWT: buildMockClaimsRoot(ctrl),
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid authorization target user: a group allowlist exists, but root is not a member of any defined group")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			u, err := processFlow(tt.cfg, tt.env, tt.valid, tt.authFlow)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}

			if tt.assertContext != nil {
				tt.assertContext(t, u)
			}
		})
	}
}

func Test_processFromState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	usr, err := user.Current()
	assert.NoError(t, err, "unable to prepare for test, user.Current()")

	workingState := envparser.StatefulEnv{
		Username:  usr.Username,
		BaseDir:   "/ecp",
		BuildsDir: "/ecp/builds",
		CacheDir:  "/ecp/cache",
	}

	type args struct {
		state envparser.StatefulEnv
	}
	tests := []struct {
		name    string
		args    args
		want    UserContext
		wantErr bool
	}{
		{
			name: "Invalid user",
			args: args{
				state: envparser.StatefulEnv{
					Username: "invalid user",
				},
			},
			want:    UserContext{},
			wantErr: true,
		}, {
			name: "Valid return",
			args: args{
				state: workingState,
			},
			want: UserContext{
				homeDir:   usr.HomeDir,
				uid:       usr.Uid,
				gid:       usr.Gid,
				username:  workingState.Username,
				baseDir:   workingState.BaseDir,
				buildsDir: workingState.BuildsDir,
				cacheDir:  workingState.CacheDir,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := processFromState(tt.args.state, sysLog)
			if (err != nil) != tt.wantErr {
				t.Errorf("ProcessFromState() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ProcessFromState() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserContext_checkLists(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	cfg := buildMockFullCfg(ctrl)

	t.Run("invalid user lookup", func(t *testing.T) {
		v := &UserContext{
			username: "invalid user",
		}

		err := v.checkLists(cfg.Auth())
		assert.Error(t, err)
	})
}

func TestUserContext_IntegerID(t *testing.T) {
	type fields struct {
		UID string
		GID string
	}
	tests := []struct {
		name    string
		fields  fields
		want    int
		want1   int
		wantErr bool
	}{
		{
			name:    "Valid integers",
			fields:  fields{"100", "200"},
			want:    100,
			want1:   200,
			wantErr: false,
		}, {
			name:    "Invalid UID",
			fields:  fields{"a", "200"},
			want:    -1,
			want1:   -1,
			wantErr: true,
		}, {
			name:    "Invalid GID",
			fields:  fields{"100", "a"},
			want:    -1,
			want1:   -1,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := &UserContext{
				uid: tt.fields.UID,
				gid: tt.fields.GID,
			}
			got, got1, err := v.IntegerID()
			if (err != nil) != tt.wantErr {
				t.Errorf("UserContext.IntegerID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("UserContext.IntegerID() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("UserContext.IntegerID() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestUserContext_validateUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockFailRA := mock_validation.NewMockRunAsValidator(ctrl)
	mockFailRA.EXPECT().Execute(gomock.Any(), currentUsr.Username).Return(
		jacamarplugins.RunAsOverride{},
		errors.New("runas failed"),
	).Times(1)

	mockSuccessRA := mock_validation.NewMockRunAsValidator(ctrl)
	mockSuccessRA.EXPECT().Execute(gomock.Any(), currentUsr.Username).Return(
		jacamarplugins.RunAsOverride{
			Username: currentUsr.Username,
		},
		nil,
	).Times(1)

	mockBadRA := mock_validation.NewMockRunAsValidator(ctrl)
	mockBadRA.EXPECT().Execute(gomock.Any(), currentUsr.Username).Return(
		jacamarplugins.RunAsOverride{
			Username: "nonExistentUser",
		},
		nil,
	).Times(1)

	tests := map[string]processTests{
		"valid current user, no runas enforced": {
			claims: gitlabjwt.Claims{
				UserLogin: currentUsr.Username,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertContext: func(t *testing.T, u UserContext) {
				assert.Equal(t, currentUsr.Username, u.username, "target username")
				assert.Equal(t, currentUsr.Uid, u.uid, "target UID")
				assert.Equal(t, "", u.baseDir, "no basedir should be defined at this point")
			},
		},
		"first checklist fails": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to identify (user.Lookup) on , please verify their existence on the system",
				)
			},
		},
		"runas enabled, failed validate": {
			claims: gitlabjwt.Claims{
				UserLogin: currentUsr.Username,
			},
			valid: validators{
				runAs: mockFailRA,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "runas failed")
			},
		},
		"runas enabled, successfully user identified": {
			claims: gitlabjwt.Claims{
				UserLogin: currentUsr.Username,
			},
			valid: validators{
				runAs: mockSuccessRA,
			},
			assertError: tst.AssertNoError,
		},
		"runas enabled, second checklists fails": {
			claims: gitlabjwt.Claims{
				UserLogin: currentUsr.Username,
			},
			valid: validators{
				runAs: mockBadRA,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to identify (user.Lookup) on nonExistentUser, please verify their existence on the system",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			u := &UserContext{}
			err := u.validateUser(tt.auth, tt.valid, tt.claims)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}

			if tt.assertContext != nil {
				tt.assertContext(t, *u)
			}
		})
	}
}

func TestUserContext_validateCurrentUser(t *testing.T) {
	t.Run("successfully generated user context", func(t *testing.T) {
		u := &UserContext{}

		err := u.validateCurrentUser()
		assert.NoError(t, err)

		usr, _ := user.Current()
		assert.Equal(t, usr.Username, u.username, "excepted username")
		assert.Equal(t, usr.Gid, u.gid, "excepted GID")
		assert.Equal(t, usr.Uid, u.uid, "excepted UID")
	})
}
