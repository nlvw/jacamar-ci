package authuser

import (
	"io/ioutil"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
)

var testRedEnv envparser.RequiredEnv
var testRedClaims gitlabjwt.Claims
var currentUsr *user.User

func buildMockFullCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{
		UserAllowlist:  []string{"u1", "u4", "u8"},
		UserBlocklist:  []string{"u3", "u4", "u8"},
		GroupAllowlist: []string{"g2", "g5", "g8"},
		GroupBlocklist: []string{"g7", "g8"},
	}).AnyTimes()
	m.EXPECT().General().Return(configure.General{
		DataDir: "/ci",
	}).AnyTimes()

	return m
}

func buildMockPreListsCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{
		ListsPreValidation: true,
		UserAllowlist:      []string{"u1", "u4", "u8"},
		UserBlocklist:      []string{"u3", "u4", "u8"},
		GroupAllowlist:     []string{"g2", "g5", "g8"},
		GroupBlocklist:     []string{"g7", "g8"},
	}).AnyTimes()
	m.EXPECT().General().Return(configure.General{
		DataDir: "/ci",
	}).AnyTimes()

	return m
}

func buildMockMinCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{
		Downscope: "setuid",
	}).AnyTimes()
	m.EXPECT().General().Return(configure.General{
		DataDir: "/ci",
	}).AnyTimes()

	return m
}

func buildMissDirCfg(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()
	m.EXPECT().General().Return(configure.General{}).AnyTimes()

	return m
}

func Test_Authorized_Interface(t *testing.T) {
	u := UserContext{
		username:  "username",
		homeDir:   "homeDir",
		uid:       "1000",
		gid:       "1000",
		baseDir:   "baseDir",
		buildsDir: "buildsDir",
		cacheDir:  "cacheDir",
		scriptDir: "scriptDir",
	}

	assert.Equal(t, "username", u.Username(), "Username()")
	assert.Equal(t, "homeDir", u.HomeDir(), "HomeDir()")
	assert.Equal(t, "baseDir", u.BaseDir(), "BaseDir()")
	assert.Equal(t, "buildsDir", u.BuildsDir(), "BuildsDir()")
	assert.Equal(t, "cacheDir", u.CacheDir(), "CacheDir()")
	assert.Equal(t, "scriptDir", u.ScriptDir(), "ScriptDir()")
	assert.Equal(t, envparser.StatefulEnv{
		BaseDir:   "baseDir",
		BuildsDir: "buildsDir",
		CacheDir:  "cacheDir",
		ScriptDir: "scriptDir",
		Username:  "username",
	}, u.BuildState(), "BuildState()")
	assert.Equal(t, "Running as username UID: 1000 GID: 1000\n", u.PrepareNotification(), "PrepareNotification()")
	assert.False(t, u.SharedBuildsDir(), "shared builds directory should always remain false")
}

func Test_CurrentUser(t *testing.T) {
	// At this time the CurrentUser function should simply defer to calling the
	// associated method in the user package. Enforce this until future changes may be made.
	sysUsr, sysErr := user.Current()
	usr, err := CurrentUser()

	assert.Equal(t, sysUsr, usr)
	assert.Equal(t, sysErr, err)
}

func TestNewAuthorizedUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	tests := map[string]processTests{
		"invalid user provided to process from state": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					Username: "something_random_123",
				},
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"valid current user provided, processed from state": {
			env: envparser.ExecutorEnv{
				// Note, we trust that the envparser will enforce required
				// stateful variables, thus we don't bother to verify they exist.
				StatefulEnv: envparser.StatefulEnv{
					Username: currentUsr.Username,
					BaseDir:  "/base/dir",
				},
			},
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAuthorized: func(t *testing.T, auth Authorized) {
				assert.Equal(t, currentUsr.Username, auth.Username())
				assert.Equal(t, "/base/dir", auth.BaseDir())
				assert.Equal(t, "", auth.ScriptDir())
			},
		},
		"invalid process flow, no valid job jwt provided": {
			env:      envparser.ExecutorEnv{},
			cfg:      buildMissDirCfg(ctrl),
			authFlow: true,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		// There could be efforts here to improve authuser wide package testing;
		// however, you will need to slightly refactor how the validators
		// are identified in order to mock at this level.
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := NewAuthorizedUser(tt.cfg, tt.env, sysLog, tt.authFlow)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuthorized != nil {
				tt.assertAuthorized(t, got)
			}
		})
	}
}

func init() {
	testRedEnv = envparser.RequiredEnv{
		ConcurrentID: "0",
		JobID:        "123",
		RunnerShort:  "short",
	}
	testRedClaims = gitlabjwt.Claims{
		ProjectPath: "group/project",
		UserLogin:   "tester",
	}
	currentUsr, _ = user.Current()
}
