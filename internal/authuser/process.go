package authuser

import (
	"fmt"
	"os/user"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

// processFlow manages all aspects of the user authorization flow, leveraging the supplied
// configuration as well as context from the custom executor. The process supporting this will
// ensure the entire authorization flow is strictly observed, as  such it is recommended to
// leverage stateful variables to store key aspects of the user context after a successful
// authorization. Any issues encountered during this process will results in an error returned.
// Though steps are taken to return a safe error externally defined scripts/syscall are made
// that may return more information than required. It is strongly advised that the error message
// presented to a CI user only inform them of the general failures and more details are logged
// by the privileged user.
func processFlow(
	cfg configure.Configurer,
	env envparser.ExecutorEnv,
	valid validators,
	authFlow bool,
) (UserContext, error) {
	u := &UserContext{}

	auth := cfg.Auth()
	jwtClaims, err := valid.jobJWT.Parse()
	if err != nil {
		return UserContext{}, fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}

	if authFlow {
		err := u.validateUser(auth, valid, jwtClaims)
		if err != nil {
			return UserContext{}, fmt.Errorf("invalid authorization target user: %w", err)
		}

		// Root can be a valid GitLab account and should not be allowed to run while auth is
		// enabled and a non standard downscope has been identified we enforce not using it
		// of the current service account.
		if err := u.checkUserContext(auth); err != nil {
			return UserContext{}, err
		}
	} else {
		if err := u.validateCurrentUser(); err != nil {
			return UserContext{}, fmt.Errorf("unable to validate current user: %w", err)
		}
	}

	if err := u.identifyDirectories(cfg.General(), env.RequiredEnv, jwtClaims); err != nil {
		return UserContext{}, fmt.Errorf("unable to identify target directories: %w", err)
	}

	return *u, nil
}

// processFromState returns a UserContext struct by leveraging the stateful variables established
// during the config stage and provided to subsequent stages by the custom executor. No auth data
// is reconfirmed as part of this process.
func processFromState(s envparser.StatefulEnv, sysLog *logrus.Entry) (UserContext, error) {
	usr, err := user.Lookup(s.Username)
	if err != nil {
		return UserContext{}, err
	}

	sysLog.Debug("user context " + s.Username + " processed from job state")

	return UserContext{
		homeDir:   usr.HomeDir,
		uid:       usr.Uid,
		gid:       usr.Gid,
		username:  s.Username,
		baseDir:   s.BaseDir,
		buildsDir: s.BuildsDir,
		cacheDir:  s.CacheDir,
		scriptDir: s.ScriptDir,
	}, nil
}

// validateCurrentUser updates required UserContext accordingly. This it to
// be used when authorization flow is not enabled.
func (u *UserContext) validateCurrentUser() error {
	usr, err := user.Current()
	if err != nil {
		return err
	}

	u.username = usr.Username
	u.homeDir = usr.HomeDir
	u.uid = usr.Uid
	u.gid = usr.Gid

	return nil
}

func (u *UserContext) validateUser(
	auth configure.Auth,
	valid validators,
	jwtClaims gitlabjwt.Claims,
) error {
	u.username = jwtClaims.UserLogin

	if auth.ListsPreValidation {
		if err := u.checkLists(auth); err != nil {
			return err
		}
	}

	if valid.runAs != nil {
		if err := u.processRunAs(jwtClaims, valid.runAs); err != nil {
			return err
		}
	}

	// Check defined allowlist or blocklist (final uid before job execution).
	if err := u.checkLists(auth); err != nil {
		return err
	}

	return nil
}

func (u *UserContext) processRunAs(
	jwtClaims gitlabjwt.Claims,
	vra validation.RunAsValidator,
) error {
	override, err := vra.Execute(jwtClaims, u.username)
	if err != nil {
		return err
	}

	u.username = override.Username

	return nil
}

// checkLists verifies all allowlist/blocklist logic is observed. It in turn ensures
// aspects that the UserContext has been updated (username, uid, gid, and homedir only).
func (u *UserContext) checkLists(auth configure.Auth) error {
	target, groups, err := lookupUser(u.username)
	if err != nil {
		return err
	}

	// verify shell allowlist
	if err := validateShell(auth.ShellAllowlist, target.Uid, target.Username); err != nil {
		return err
	}

	if err := validateABLists(auth, target, groups); err != nil {
		return err
	}

	u.uid = target.Uid
	u.gid = target.Gid
	u.homeDir = target.HomeDir

	return nil
}

// lookupUser checks the local system for the declared user and group memberships.
func lookupUser(username string) (usr *user.User, groups []string, err error) {
	usr, err = user.Lookup(username)
	if err != nil {
		return nil, nil, fmt.Errorf(
			"unable to identify (user.Lookup) on %s, please verify their existence on the system",
			username,
		)
	}

	groupids, err := usr.GroupIds()
	if err != nil {
		return nil, nil, fmt.Errorf(
			"unable to identify groups (usr.GroupIds) that user %s is a member of",
			username,
		)
	}
	for _, gid := range groupids {
		groupptr, err := user.LookupGroupId(gid)
		if err != nil {
			return nil, []string{}, fmt.Errorf("unable to lookup group id %s: %w", gid, err)
		}
		group := *groupptr
		groups = append(groups, group.Name)
	}

	return
}

func (u *UserContext) checkUserContext(auth configure.Auth) error {
	if auth.Downscope != "" {
		usr, err := user.Current()
		if err != nil {
			return fmt.Errorf("unable to identify service user: %w", err)
		}

		if u.username == "root" || u.uid == "0" || u.username == usr.Username {
			return fmt.Errorf("cannot execute as %s while authorization is enforced", u.username)
		}
	}

	return nil
}
