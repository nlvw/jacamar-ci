// Package authuser (authorize user) maintains the complete authorization flow, leveraging
// the job's configuration as well as context provided by the custom executor to ensure a
// fully authorized user is identified for the CI job.
package authuser

import (
	"fmt"
	"os/user"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/gitlabjwt"
)

// Authorized implements an interface allowing read-only access to the established
// user context. All information is identified during a job's configuration stage
// and remains consistent throughout the life of the job.
type Authorized interface {
	Username() string
	HomeDir() string
	IntegerID() (uid int, gid int, err error)
	BaseDir() string
	BuildsDir() string
	CacheDir() string
	ScriptDir() string
	SharedBuildsDir() bool
	BuildState() envparser.StatefulEnv
	PrepareNotification() string
}

// UserContext contains validated user details for the current CI job.
type UserContext struct {
	username  string // Local target username.
	homeDir   string // Local job user's home directory.
	uid       string // Local user identifier.
	gid       string // Local group identifier.
	baseDir   string // Base directory for CI job and command interactions.
	buildsDir string // Working directory where the job will be created.
	cacheDir  string // Directory where local cache will be stored.
	scriptDir string // Location for script generation and command execution.
}

type validators struct {
	runAs  validation.RunAsValidator
	jobJWT gitlabjwt.Validator
}

// Username for the validated user.
func (u UserContext) Username() string {
	return u.username
}

// HomeDir for the validate user.
func (u UserContext) HomeDir() string {
	return u.homeDir
}

// IntegerID returns the valid user's UID, GID as integer values.
func (u UserContext) IntegerID() (uid int, gid int, err error) {
	uid, err = strconv.Atoi(u.uid)
	if err != nil {
		return -1, -1, fmt.Errorf(
			fmt.Sprintf("unable to convert valid user's UID (%s)", u.uid),
		)
	}
	gid, err = strconv.Atoi(u.gid)
	if err != nil {
		return -1, -1, fmt.Errorf(
			fmt.Sprintf("unable to convert valid user's GID (%s)", u.gid),
		)
	}
	return
}

// BaseDir directory for CI job and command interactions.
func (u UserContext) BaseDir() string {
	return u.baseDir
}

// BuildsDir is the working directory created on the local file system.
func (u UserContext) BuildsDir() string {
	return u.buildsDir
}

// CacheDir is the working directory created on the local file system.
func (u UserContext) CacheDir() string {
	return u.cacheDir
}

// ScriptDir is the directory for script storage and command execution.
func (u UserContext) ScriptDir() string {
	return u.scriptDir
}

// SharedBuildsDir whether the builds directory is shared between concurrent jobs.
func (u UserContext) SharedBuildsDir() bool {
	return false
}

// BuildState populates and return the StatefulEnv.
func (u UserContext) BuildState() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		Username:  u.username,
		BaseDir:   u.baseDir,
		BuildsDir: u.buildsDir,
		CacheDir:  u.cacheDir,
		ScriptDir: u.scriptDir,
	}
}

// PrepareNotification returns details on the current job's validated user to
// used in the prepare message.
func (u UserContext) PrepareNotification() string {
	return fmt.Sprintf("Running as %s UID: %s GID: %s\n", u.username, u.uid, u.gid)
}

// CurrentUser returns the current user.
func CurrentUser() (*user.User, error) {
	return user.Current()
}

func establishValidators(
	cfg configure.Configurer,
	env envparser.ExecutorEnv,
) (validators, error) {
	rs, err := validation.NewRunAs(cfg.Auth())
	if err != nil {
		return validators{}, fmt.Errorf("failed to establish RunAs validator: %w", err)
	}

	return validators{
		runAs: rs,
		jobJWT: gitlabjwt.Factory(
			env.RequiredEnv.CIJobJWT,
			env.RequiredEnv.JobID,
			env.RequiredEnv.ServerURL,
		),
	}, nil
}

// NewAuthorizedUser processes and identifies a local user for the GitLab CI job. The
// current environment and configuration are used to dictate how the user authorization process
// will be handled. A boolean must be provided in order to convey if the authorization flow
// is executed (job state cannot be established). Once the process is completed the user context is
// immutable and should only be accessed through th0e provided interface.
func NewAuthorizedUser(
	cfg configure.Configurer,
	env envparser.ExecutorEnv,
	sysLog *logrus.Entry,
	authFlow bool,
) (Authorized, error) {
	if env.StatefulEnv.Username != "" {
		return processFromState(env.StatefulEnv, sysLog)
	}

	vs, err := establishValidators(cfg, env)
	if err != nil {
		sysLog.Error(err.Error())
		return nil, err
	}

	auth, err := processFlow(cfg, env, vs, authFlow)
	if err != nil {
		sysLog.Error(strings.Join([]string{
			"Failed to authorize user for CI job execution: ", err.Error(),
		}, ""))
	} else {
		sysLog.Info(strings.Join([]string{
			"User ", auth.Username(), " authorized for CI job execution.",
		}, ""))
	}
	return auth, err
}
