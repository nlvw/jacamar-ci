package batch

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

const (
	// DefaultSleep is the duration to wait between scheduler commands/interactions.
	DefaultSleep = 30 * time.Second
)

// Manager defines a shared set of procedures that can be leveraged by any
// supported batch executor.
type Manager interface {
	BatchCmd() string
	StateCmd() string
	StopCmd() string
	UserArgs() string
	NFSTimeout(string, logging.Messenger)
	TailFiles([]string, chan struct{}, time.Duration, logging.Messenger) error
}

// Settings represents generic details that can apply to all job's that are planned
// to be submitted to a supported underlying scheduling system.
type Settings struct {
	// BatchCmd required for submission to the scheduler.
	BatchCmd string
	// StateCmd to gather job state.
	StateCmd string
	// StopCmd to cancel a requested allocation.
	StopCmd string
	// IllegalArgs reserved by the executor.
	IllegalArgs []string
}

// Job represents details/configurations associated with any batch executor
// that are established at the start of a CI job.
type Job struct {
	batchCmd    string
	stateCmd    string
	stopCmd     string
	illegalArgs []string
	userArgs    []string // Arguments provided by the user.
}

// BatchCmd returns the command for submitting to the scheduler.
func (j Job) BatchCmd() string {
	return j.batchCmd
}

// StateCmd returns the command to obtaining a job's status from the scheduler.
func (j Job) StateCmd() string {
	return j.stateCmd
}

// StopCmd returns the command used to stop a currently scheduled job.
func (j Job) StopCmd() string {
	return j.stopCmd
}

// UserArgs returns all user defined argument for submission to the scheduler.
func (j Job) UserArgs() string {
	return strings.Join(j.userArgs, " ")
}

// NFSTimeout checks the validity of the duration string supplied and pauses the current
// routine for  that period of time. Please note this is a measure to account for the
// differences presented by site network file systems.
func (j Job) NFSTimeout(duration string, msg logging.Messenger) {
	if duration != "" {
		timeout, err := time.ParseDuration(duration)
		if err != nil {
			msg.Warn(
				"Invalid nfs_timeout configuration specified by runner configuration: %s",
				err.Error(),
			)
		}
		time.Sleep(timeout)
	}
}

// PrefixSchedulerPath returns the full path to an application by checking for the admin
// defined SchedulerBin. THe validity of the SchedulerBin is not checked.
func PrefixSchedulerPath(app, path string) string {
	if strings.HasSuffix(path, "/") {
		return fmt.Sprintf("%s%s", path, app)
	} else if path != "" {
		return fmt.Sprintf("%s/%s", path, app)
	}

	return app
}

// checkIllegalArgs compares the provided list of arguments against the scheduler  system defined
// list of illegal arguments. A warning message is returned.
func (j Job) checkIllegalArgs(msg logging.Messenger) {
	var warnings string
	for i := range j.userArgs {
		for k := range j.illegalArgs {
			if strings.HasPrefix(j.userArgs[i], j.illegalArgs[k]) {
				warnings = warnings + " " + j.userArgs[i]
			}
		}
	}

	if warnings != "" {
		msg.Warn("Illegal argument detected. Please remove:" + warnings)
	}
}

// identifyUserArgs parses an environment provided by the custom executor. Will observer default
// SCHEDULER_PARAMETER value and warn users if nothing is found.
func identifyUserArgs(keys []string, msg logging.Messenger) []string {
	// Default to "SCHEDULER_PARAMETERS" always established.
	keys = append(keys, "SCHEDULER_PARAMETERS")

	for _, k := range keys {
		v, f := os.LookupEnv(envparser.UserEnvPrefix + k)
		if f {
			return strings.Split(v, " ")
		}
	}

	msg.Warn(
		"No %s variable detected, please check your CI job if this is unexpected.",
		keys[0],
	)
	return []string{}
}

// NewBatchJob uses the configuration and baseline Settings to establish an interface for
// managing a scheduled CI job. At the same time warning messages will be created when
// potentially undesirable states (e.g. illegal arguments or missing parameters)
// are encountered.
func NewBatchJob(s Settings, opt configure.Batch, msg logging.Messenger) Manager {
	mgr := Job{
		batchCmd:    PrefixSchedulerPath(s.BatchCmd, opt.SchedulerBin),
		stateCmd:    PrefixSchedulerPath(s.StateCmd, opt.SchedulerBin),
		stopCmd:     PrefixSchedulerPath(s.StopCmd, opt.SchedulerBin),
		illegalArgs: s.IllegalArgs,
		userArgs:    identifyUserArgs(opt.ArgumentsVariable, msg),
	}
	mgr.checkIllegalArgs(msg)

	return mgr
}
