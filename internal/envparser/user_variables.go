package envparser

import (
	"errors"
	"os"
	"regexp"
	"strings"
)

// TrustJobToken returns the CI Job Token as provided by the runner.
func TrustJobToken() string {
	return os.Getenv("TRUSTED_CI_JOB_TOKEN")
}

// NoBashProfile returns a boolean if the CI user has specified they are expecting a baseline
// bash environment created with the '--noprofile' argument.
func NoBashProfile() bool {
	return trueEnvVar(UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE")
}

// EtcProfileOnly returns a boolean if the CI user has specified they are expecting a baseline
// bash environment created with the '--noprofile' argument and further ensuring /etc/profile
// is sourced prior to script execution.
func EtcProfileOnly() bool {
	return trueEnvVar(UserEnvPrefix + "JACAMAR_ETC_PROFILE_ONLY")
}

// check if variable 'true' or '1'.
func trueEnvVar(key string) bool {
	v := os.Getenv(key)
	v = strings.ToLower(strings.TrimSpace(v))
	if v == "true" || v == "1" {
		return true
	}
	return false
}

// verifyJWTFromat checks the encoded JWT against regular expression and
// returns if conditions meet.
func verifyJWTFromat() (string, error) {
	encoded, found := os.LookupEnv(UserEnvPrefix + "CI_JOB_JWT")
	// JWT is not required in all cases, when required and not present
	// it'll fail later with a more helpful error message.
	if found {
		re := regexp.MustCompile(`^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]*$`)
		if !re.MatchString(encoded) {
			return "", errors.New(
				"non-standard CI_JOB_JWT detected in environment, do not change this variable",
			)
		}
	}
	return encoded, nil
}
