package utils

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/BurntSushi/toml"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
)

type customBuildDir struct {
	Enabled bool `toml:"enabled"`
}

type runnerSettings struct {
	Executor       string          `toml:"executor"`
	CustomBuildDir *customBuildDir `toml:"custom_build_dir"`
}

type runnerConfig struct {
	SetUIDUserWhitelist  []string `toml:"setuid_user_whitelist"`
	SetUIDUserBlacklist  []string `toml:"setuid_user_blacklist"`
	SetUIDGroupWhitelist []string `toml:"setuid_groups_whitelist"`
	SetUIDGroupBlacklist []string `toml:"setuid_groups_blacklist"`
	ShellWhitelist       []string `toml:"shell_whitelist"`

	runnerSettings

	Name                     string `toml:"name"`
	SetUIDDataDir            string `toml:"setuid_data_dir"`
	SetUIDValidateRunAs      string `toml:"setuid_validate_run_as"`
	SetUIDRunAsUser          string `toml:"setuid_run_as_user"`
	BatchSystem              string `toml:"batch_system"`
	BatchRequiredSpecs       string `toml:"batch_required_args"`
	BatchSchedulerParameters string `toml:"batch_scheduler_parameters"`
	NFSTimeout               string `toml:"nfs_timeout"`
	SchedulerBin             string `toml:"scheduler_bin"`

	SetUID              bool `toml:"setuid"`
	Federated           bool `toml:"federated"`
	RetainSchedulerLogs bool `toml:"retain_scheduler_logs"`
	HideSensitiveEnv    bool `toml:"hide_sensitive_env"`
}

type gitlabCfg struct {
	Runners []*runnerConfig `toml:"runners" json:"runners"`
}

func translateConfiguration(arg arguments.TranslateCmd) error {
	glCfg, err := loadGitLabCfg(arg.Source)
	if err != nil {
		return err
	}

	msg.Notify("\nCurrently configured runners\n")
	for i, r := range glCfg.Runners {
		msg.Stdout(fmt.Sprintf("%d - %s", i, r.Name))
	}

	msg.Stdout("Select index of runner: ")
	var in int
	err = scanner(&in)
	if err != nil {
		return err
	}

	msg.Notify("\nBeginning translation process...")
	jacamarCfg := generateJacamarCfg(*glCfg.Runners[in])
	msg.Notify("Process completed\n")

	if arg.Target == "" {
		path, _ := os.Getwd()
		arg.Target = fmt.Sprintf("%s/jacamar-config-%v.toml", path, time.Now().Unix())
	}
	buf := new(bytes.Buffer)
	if err = toml.NewEncoder(buf).Encode(jacamarCfg); err != nil {
		return err
	}
	b, _ := ioutil.ReadAll(buf)
	if err = createFile(b, arg.Target); err != nil {
		return err
	}
	msg.Notify("Configuration file created: " + arg.Target)

	return nil
}

func loadGitLabCfg(file string) (cfg gitlabCfg, err error) {
	var r []byte
	file, _ = filepath.Abs(file)
	/* #nosec */
	// variable file path required
	r, err = ioutil.ReadFile(file)
	if err != nil {
		return
	}

	nr := strings.NewReader(string(r))
	_, err = toml.DecodeReader(nr, &cfg)
	return
}

func generateJacamarCfg(cur runnerConfig) (opt configure.Options) {
	opt.General.Name = cur.Name

	// handle executor
	optExecutor(cur, &opt)

	// handle data_dir
	opt.General.DataDir = cur.SetUIDDataDir
	if opt.General.DataDir == "" {
		msg.Warn("A data directory must be specified in order for Jacamar CI to function," +
			"please address manually ([genera].data_dir)")
	}

	// handle setuid
	optSetuid(cur, &opt)

	// misc
	if cur.CustomBuildDir != nil {
		opt.General.CustomBuildDir = cur.CustomBuildDir.Enabled
	}

	return
}

func optSetuid(cur runnerConfig, opt *configure.Options) {
	// Note, though not required now, setuid used to be needed before
	// any "authorization" like processes would take place.
	if cur.SetUID {
		opt.Auth.Downscope = "setuid"

		// runas
		opt.Auth.RunAs.ValidationScript = cur.SetUIDValidateRunAs
		opt.Auth.RunAs.RunAsVariable = cur.SetUIDRunAsUser

		// federation
		opt.Auth.RunAs.Federated = cur.Federated

		// lists
		opt.Auth.GroupBlocklist = cur.SetUIDGroupBlacklist
		opt.Auth.GroupAllowlist = cur.SetUIDGroupWhitelist
		opt.Auth.UserBlocklist = cur.SetUIDUserBlacklist
		opt.Auth.UserAllowlist = cur.SetUIDUserWhitelist
		opt.Auth.ShellAllowlist = cur.ShellWhitelist
	}

	if cur.HideSensitiveEnv {
		msg.Warn("HideSensitiveEnv is not supported by Jacamar")
	}
}

func optExecutor(cur runnerConfig, opt *configure.Options) {
	executor := strings.ToLower(cur.Executor)
	if executor == "batch" || executor == "shell" {
		opt.General.Executor = executor
		if executor == "batch" {
			if cur.BatchSystem != "" {
				opt.General.Executor = strings.ToLower(cur.BatchSystem)
			} else {
				opt.General.Executor = "cobalt || lsf || slurm"
				msg.Warn(
					"Unable to identify batch system, address manually ([genera].executor)",
				)
			}
			opt.Batch.ArgumentsVariable = []string{cur.BatchSchedulerParameters}
			opt.Batch.NFSTimeout = cur.NFSTimeout
			opt.Batch.SchedulerBin = cur.SchedulerBin
			opt.General.RetainLogs = cur.RetainSchedulerLogs

			if cur.BatchRequiredSpecs != "" {
				msg.Warn("BatchRequiredSpecs is not supported by Jacamar")
			}
		}
	} else {
		failure("invalid executor detected, must be either shell or batch executor")
	}
}
