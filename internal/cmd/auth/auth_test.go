package auth

import (
	"errors"
	"os"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type authTests struct {
	ae        *executors.AbstractExecutor
	c         arguments.ConcreteArgs
	opt       configure.Auth
	targetEnv map[string]string

	auth *mock_authuser.MockAuthorized
	msg  *mock_logging.MockMessenger

	assertError    func(*testing.T, error)
	assertAbstract func(*testing.T, *executors.AbstractExecutor)
	assertDir      func(*testing.T, string)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
		"TRUSTED_CI_JOB_ID":                    "123",
		"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
		"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
		envparser.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                "gitlab.url",
	}
}

func Test_establishAbstract(t *testing.T) {
	var tarErr errorhandling.ObfuscatedErr

	done := make(chan struct{})
	defer close(done)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	tests := map[string]authTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			got, err := establishAbstract(tt.c, tt.msg, done)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, got)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_runMechanism(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "none",
		},
	).Times(1)
	none.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).Times(1)

	missing := mock_configure.NewMockConfigurer(ctrl)
	missing.EXPECT().Auth().Return(
		configure.Auth{},
	).Times(1)
	missing.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).Times(1)

	setgid := mock_configure.NewMockConfigurer(ctrl)
	setgid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setgid",
		},
	).Times(1)
	setgid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).Times(1)

	setuid := mock_configure.NewMockConfigurer(ctrl)
	setuid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setuid",
		},
	).AnyTimes()
	setuid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	pass := mock_authuser.NewMockAuthorized(ctrl)
	pass.EXPECT().Username().Return("user").Times(1)
	pass.EXPECT().HomeDir().Return("/home/user").Times(1)
	pass.EXPECT().BuildsDir().Return("/home/user/builds").Times(2)
	pass.EXPECT().CacheDir().Return("/home/user/cache").Times(2)
	pass.EXPECT().IntegerID().Return(1000, 1000, nil).Times(1)

	fail := mock_authuser.NewMockAuthorized(ctrl)
	fail.EXPECT().Username().Return("user").AnyTimes()
	fail.EXPECT().HomeDir().Return("/home/user").AnyTimes()
	fail.EXPECT().IntegerID().Return(1000, 1000, errors.New("simulated error")).AnyTimes()

	tests := map[string]authTests{
		"no configuration, no runner created": {
			opt: configure.Auth{},
			ae: &executors.AbstractExecutor{
				Cfg: missing,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"invalid runner configuration, downscope must be defined",
				)
			},
		},
		"none downscope mechanism defined": {
			ae: &executors.AbstractExecutor{
				Cfg:  none,
				Auth: pass,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"misspelled downscope in configuration with authorization": {
			ae: &executors.AbstractExecutor{
				Cfg: setgid,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"invalid runner configuration, ensure supported downscope is defined",
				)
			},
		},
		"configuration, setuid run mechanism": {
			ae: &executors.AbstractExecutor{
				Cfg:  setuid,
				Auth: pass,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"configuration, setuid run mechanism, error encountered": {
			ae: &executors.AbstractExecutor{
				Cfg:  setuid,
				Auth: fail,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			done := make(chan struct{})
			defer close(done)

			_, err := newRunner(tt.ae, done)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_jacamarCmd(t *testing.T) {
	tests := []struct {
		name  string
		auth  configure.Auth
		stage string
		want  string
	}{
		{
			name: "source script defined in configuration, prepare stage",
			auth: configure.Auth{
				SourceScript: "/ci/profile",
			},
			stage: "prepare",
			want:  "source /ci/profile && exec jacamar prepare",
		}, {
			name: "jacamar path defined, cleanup stage",
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
			stage: "cleanup",
			want:  "exec /bin/jacamar cleanup",
		}, {
			name:  "no configuration, run stage",
			auth:  configure.Auth{},
			stage: "run script sub-stage",
			want:  "exec jacamar run script sub-stage",
		}, {
			name: "source script and path defined, config stage",
			auth: configure.Auth{
				SourceScript: "/ci/profile",
				JacamarPath:  "/bin/jacamar",
			},
			stage: "config",
			want:  "source /ci/profile && exec /bin/jacamar config",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := jacamarCmd(tt.auth, tt.stage)

			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_rootDirManagement(t *testing.T) {
	cur, _ := user.Current()
	if cur.Uid != "0" {
		t.Skip("rootDirManagement tests must be run as root")
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// create new directory
	newDir := t.TempDir()
	create := mock_authuser.NewMockAuthorized(ctrl)
	create.EXPECT().BaseDir().Return(newDir).AnyTimes()
	create.EXPECT().IntegerID().Return(0, 0, nil)

	// chmod on existing directory
	chmodDir := t.TempDir()
	_ = os.Chmod(chmodDir, 0770)
	existing := mock_authuser.NewMockAuthorized(ctrl)
	existing.EXPECT().BaseDir().Return(chmodDir).AnyTimes()
	existing.EXPECT().IntegerID().Return(0, 0, nil)

	tests := map[string]authTests{
		"create new directory": {
			auth: create,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
		"chmod on existing directory": {
			auth: existing,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := rootDirManagement(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDir != nil {
				tt.assertDir(t, tt.auth.BaseDir())
			}
		})
	}
}
