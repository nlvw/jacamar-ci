package preparations

import (
	"io/ioutil"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	jacamartst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type prepTests struct {
	ae        *executors.AbstractExecutor
	c         arguments.ConcreteArgs
	message   string
	reqBool   bool
	targetEnv map[string]string

	msg logging.Messenger

	assertAbstract func(*testing.T, *executors.AbstractExecutor)
	assertError    func(*testing.T, error)
	assertMap      func(*testing.T, map[string]string)
}

func TestStdError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr("Error encountered during job: %s", "cleanup").Times(1)
	m.EXPECT().Stderr("Error encountered during job: %s", "config").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "prepare").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "run").Times(1)

	tests := map[string]prepTests{
		"error encountered during cleanup": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			message: "cleanup",
			msg:     m,
		},
		"error encountered during run": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			message: "run",
			msg:     m,
		},
		"error encountered during prepare": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			message: "prepare",
			msg:     m,
		},
		"error encountered during config": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			message: "config",
			msg:     m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			StdError(tt.c, tt.message, tt.msg, func() {})
		})
	}
}

func TestJobContext(t *testing.T) {
	workingContext := map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":     "1",
		"TRUSTED_CI_JOB_ID":                    "123",
		"TRUSTED_CI_JOB_TOKEN":                 "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":        "abc",
		"TRUSTED_CI_BUILDS_DIR":                "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                 "/gitlab/cache",
		envparser.UserEnvPrefix + "CI_JOB_JWT": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                "gitlab.url",
	}

	tests := map[string]prepTests{
		"config_exec - invalid required environments": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/valid_exec_config.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"config_exec - invalid configuration file": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to establish Configurer: error executing ReadFile(), open /missing/file/config.toml: no such file or directory",
				)
			},
		},
		"config_exec - valid configuration and environment": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/valid_exec_config.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			jacamartst.SetEnv(tt.targetEnv)
			defer jacamartst.UnsetEnv(tt.targetEnv)

			_, err := JobContext(tt.c, func() {})
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewAuthorizedUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	curUser, _ := authuser.CurrentUser()

	ll := logrus.New()
	ll.Out = ioutil.Discard
	sysLog := logrus.NewEntry(ll)

	tests := map[string]prepTests{
		"process authorized user from available state": {
			ae: &executors.AbstractExecutor{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: curUser.Username,
					},
				},
				SysLog: sysLog,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.NotNil(t, ae)
				assert.Equal(t, curUser.Username, ae.Auth.Username())
			},
		},
		"failed to process authorized user, invalid state": {
			ae: &executors.AbstractExecutor{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "1-invalid-user",
					},
				},
				SysLog: sysLog,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			// Only state is analyzed at this time, static 'false'
			err := NewAuthorizedUser(tt.ae, false)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAbstract != nil {
				tt.assertAbstract(t, tt.ae)
			}
		})
	}
}

func Test_stateReq(t *testing.T) {
	// Maintaining the state of an authorized user as well as job specific configurations
	// is vita. We need enforce the mechanisms used to determine if state is required.

	tests := map[string]prepTests{
		"config_exec stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			reqBool: false,
		},
		"prepare_exec stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			reqBool: true,
		},
		"run_exec stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			reqBool: true,
		},
		"cleanup_exec stage, configuration defined": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "some.toml",
				},
			},
			reqBool: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := stateReq(tt.c)

			assert.Equal(t, tt.reqBool, got, "expected state?")
		})
	}
}

func Test_stageConfig(t *testing.T) {
	tests := map[string]prepTests{
		"expect config_exec configuration": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "config_exec")
			},
		},
		"expected prepare_exec configuration": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "prepare_exec")
			},
		},
		"expect run_exec configuration": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Stage:  "stage_name",
					Script: "script.bash",
				},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "stage_name")
				assert.Equal(t, ae.ScriptPath, "script.bash")
			},
		},
		"expected cleanup_exec configuration": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			assertAbstract: func(t *testing.T, ae *executors.AbstractExecutor) {
				assert.Equal(t, ae.Stage, "cleanup_exec")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ae := &executors.AbstractExecutor{}
			stageConfig(ae, tt.c)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, ae)
			}
		})
	}
}
