// Package preparations realizes universal actions taken by different
// Jacamar applications / sub-commands to prepare for an upcoming
// CI job. Additionally stage specific triggers to the appropriate
// execution layer can be maintained here. Distinctions in workflows
// should be realized in the appropriate command package.
package preparations

import (
	"fmt"
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

// StdError output provided message using mechanism appropriate to the current stage.
func StdError(c arguments.ConcreteArgs, message string, msg logging.Messenger, exit func()) {
	prefix := "Error encountered during job: %s"
	if c.Run != nil || c.Prepare != nil {
		msg.Warn(prefix, message)
	} else {
		msg.Stderr(prefix, message)
	}
	exit()
}

// JobContext generates AbstractExecutor struct with available context provided by the custom
// executor providing an established Configurer, ExecutorEnv, and stage specific values. All rules
// relating to logging (via Logrus) are also established.
func JobContext(c arguments.ConcreteArgs, exit func()) (*executors.AbstractExecutor, error) {
	cfg, err := newConfigurer(c)
	if err != nil {
		return nil, fmt.Errorf("unable to establish Configurer: %w", err)
	}

	env, err := newExecutorEnv(stateReq(c))
	if err != nil {
		return nil, fmt.Errorf("unable to identify ExecutorEnv: %w", err)
	}

	ae := &executors.AbstractExecutor{
		Cfg: cfg,
		Env: env,
	}

	stageConfig(ae, c)
	execOptions(ae, c, exit)

	sysLog, err := logging.EstablishLogger(ae.Stage, ae.Cfg, ae.Env.RequiredEnv)
	ae.SysLog = sysLog

	return ae, err
}

// NewAuthorizedUser identifies required user context and updates the passed
// AbstractExecutor. It is expected that the JobContext has been identified prior.
func NewAuthorizedUser(ae *executors.AbstractExecutor, authFlow bool) error {
	auth, err := authuser.NewAuthorizedUser(ae.Cfg, ae.Env, ae.SysLog, authFlow)
	ae.Auth = auth
	return err
}

func newConfigurer(c arguments.ConcreteArgs) (configure.Configurer, error) {
	_, found := os.LookupEnv(configure.EnvVariable)
	if c.Config != nil {
		return configure.NewConfig(c.Config.Configuration)
	} else if !found && c.Cleanup != nil {
		return configure.NewConfig(c.Cleanup.Configuration)
	}
	return configure.NewConfig(configure.EnvVariable)
}

func newExecutorEnv(stateful bool) (env envparser.ExecutorEnv, err error) {
	env, err = envparser.Fetcher(stateful)
	if err != nil {
		return envparser.ExecutorEnv{}, fmt.Errorf(
			"unable to parse runner environment: %w", err,
		)
	}
	return
}

// stateReq determines based upon stage if job (stateful) variables should be relied upon.
func stateReq(c arguments.ConcreteArgs) bool {
	_, found := os.LookupEnv("JACAMAR_CI_BASE_DIR")
	if c.Config != nil {
		return false
	} else if c.Cleanup != nil && !found {
		return false
	}
	return true
}

// stageConfig realizes stage specific configuration not maintained in other factories.
func stageConfig(ae *executors.AbstractExecutor, c arguments.ConcreteArgs) {
	switch {
	case c.Config != nil:
		ae.Stage = "config_exec"
	case c.Prepare != nil:
		ae.Stage = "prepare_exec"
	case c.Run != nil:
		ae.Stage = c.Run.Stage
		ae.ScriptPath = c.Run.Script
	case c.Cleanup != nil:
		ae.Stage = "cleanup_exec"
	}
}

// execOptions realizes the scoping and establishment of the configuration
// environment variable for sharing the contents of a --configuration
// amongst stages.
func execOptions(ae *executors.AbstractExecutor, c arguments.ConcreteArgs, exit func()) {
	req := true
	if c.Config != nil {
		req = false
	}

	ec, err := ae.Cfg.PrepareState(req)
	if err != nil {
		StdError(
			c,
			"unable to PrepareState for configuration (--configuration), please verify encoding",
			ae.Msg,
			exit,
		)
	}
	_ = os.Setenv(configure.EnvVariable, ec)
	ae.ExecOptions = ec
}
