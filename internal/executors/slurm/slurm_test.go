package slurm

import (
	"errors"
	"path/filepath"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type slurmTests struct {
	stdin   string
	jobID   string
	cmd     string
	timeout time.Duration

	wantErr bool
	mng     *mock_batch.MockManager
	run     *mock_runmechanisms.MockRunner

	assertErrorMsg func(t *testing.T, err error)
}

func Test_executor_Prepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	miss := mock_configure.NewMockConfigurer(ctrl)
	miss.EXPECT().Batch().Return(configure.Batch{})

	path, _ := filepath.Abs("../../../test/scripts/schedulers")
	found := mock_configure.NewMockConfigurer(ctrl)
	found.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: path,
	})

	tests := map[string]struct {
		cfg         configure.Configurer
		assertError func(*testing.T, error)
	}{
		"sbatch identified": {
			cfg: found,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"sbatch not found": {
			cfg: miss,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to locate Slurm (sbatch) in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Cfg:   tt.cfg,
					Stage: "prepare_exec",
				},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Cleanup(t *testing.T) {
	err := (&executor{}).Cleanup()
	assert.Nil(t, err, "Cleanup successfully skipped")
}

func buildMockSlurm(ctrl *gomock.Controller) *mock_runmechanisms.MockRunner {
	m := mock_runmechanisms.NewMockRunner(ctrl)

	// sacct commands
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 100")).
		Return("COMPLETED", nil)
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 200")).
		Return("CANCELLED", nil)
	pending := m.EXPECT().ReturnOutput(gomock.Eq("sacct 300")).
		Return("PENDING", nil).MaxTimes(1)
	running := m.EXPECT().ReturnOutput(gomock.Eq("sacct 300")).
		Return("RUNNING", nil).MaxTimes(1).After(pending)
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 300")).
		Return("COMPLETED", nil).After(running)
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 600")).
		Return("", errors.New("failed sacct command"))
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 700")).
		Return("COMPLETING", nil).AnyTimes()
	m.EXPECT().ReturnOutput(gomock.Eq("sacct 999")).
		Return("", nil)

	return m
}

// scancel (https://slurm.schedmd.com/scancel.html) interaction testing
func Test_Mock_Scancel(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_runmechanisms.NewMockRunner(ctrl)
	m.EXPECT().ReturnOutput(gomock.Eq("scancel 100")).Return("", nil)
	m.EXPECT().ReturnOutput(
		gomock.Eq("scancel 600")).Return(
		"scancel: error: No job identification provided",
		errors.New("error"))
	m.EXPECT().ReturnOutput(gomock.Eq("scancel 999")).Return("", nil)

	tests := map[string]slurmTests{
		"scancel successful command": {
			jobID:   "100",
			cmd:     "scancel",
			run:     m,
			wantErr: false,
		},
		"sacct failed command": {
			jobID:   "600",
			cmd:     "scancel",
			run:     m,
			wantErr: true,
			assertErrorMsg: func(t *testing.T, err error) {
				assert.Equal(t, "scancel: error: No job identification provided", err.Error())
			},
		},
		"scancel no return": {
			jobID:   "999",
			cmd:     "scancel",
			run:     m,
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := cancelJob(tt.cmd, tt.jobID, tt.run)
			assert.Equal(t, tt.wantErr, err != nil)

			if tt.assertErrorMsg != nil {
				tt.assertErrorMsg(t, err)
			}
		})
	}
}
