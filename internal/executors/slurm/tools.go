package slurm

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
)

// sbatchJobID parses stdout patterns from sbatch call to retrieve the jobID.
func sbatchJobID(out string) (string, error) {
	a := regexp.MustCompile(`[S-s]ubmitted\sbatch\sjob\s(\d+)`)
	matches := a.FindStringSubmatch(out)
	if len(matches) != 2 {
		return "", fmt.Errorf("unable to obtain jobID from sbatch stdout")
	}
	return matches[1], nil
}

func outputFile(jobID, file string) string {
	// fix up %j's in the filenames in j.out (%% is the escape for %)
	fixupJob := regexp.MustCompile(`(^|[^%])%j`)
	out := fixupJob.ReplaceAllString(file, "${1}"+jobID)
	return out
}

func sacctStdin(stateCmd, jobID string) string {
	yesterday := time.Now().AddDate(0, 0, -1)
	startDate := yesterday.Format("01/02/06")
	return fmt.Sprintf("%s -n -j %s -L --format=state -S %s", stateCmd, jobID, startDate)
}

// sacctState parses stdout from the sacct cmd to retrieve the job's state.
func sacctState(out string) string {
	lines := strings.Split(out, "\n")
	return strings.TrimSpace(lines[0])
}

// obtainSacct uses Slurm's sacct program to obtain the accounting information for both running
// and completed jobs.
func obtainSacct(
	stdin string,
	timeLimit time.Duration,
	sleepTime time.Duration,
	run runmechanisms.Runner,
) error {
	var completing *time.Timer

	time.Sleep(sleepTime) // sleep so that Slurm assigns state and job id
	out, err := run.ReturnOutput(stdin)
Loop:
	for {
		if err != nil || strings.TrimSpace(string(out)) == "" {
			return fmt.Errorf("unable to obtained output via sacct: %v", out)
		}

		switch jobState := sacctState(string(out)); jobState {
		case "PENDING", "RUNNING":
			out, err = run.ReturnOutput(stdin)
			time.Sleep(sleepTime)
		case "COMPLETING":
			if completing == nil {
				completing = time.NewTimer(timeLimit)
				defer completing.Stop()
			}
			select {
			case <-completing.C:
				return errors.New("slurm job failed, stage stuck in COMPLETING")
			default:
				out, err = run.ReturnOutput(stdin)
				time.Sleep(sleepTime)
			}
		case "COMPLETED":
			break Loop
		default: // Default will handle any other state that is considered an error.
			return fmt.Errorf("slurm job failed, final state %s", jobState)
		}
	}

	return nil
}

func (e *executor) monitorTermination(done chan struct{}) {
	for {
		select {
		case <-done:
			return
		default:
			if e.absExec.Runner.SigtermReceived() {
				if err := cancelJob(e.mng.StopCmd(), e.id, e.absExec.Runner); err != nil {
					e.absExec.Msg.Warn("Failed to cancel job (%s) %s", e.id, err.Error())
				}
				return
			}
		}
	}
}
