package shell

import (
	"errors"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type shellTests struct {
	targetPath string
	scriptPath string

	run *mock_runmechanisms.MockRunner

	assertError func(t *testing.T, err error)
}

func Test_executor_Prepare(t *testing.T) {
	tests := map[string]shellTests{
		"bash identified": {
			targetPath: os.Getenv("PATH"),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"bash not found": {
			targetPath: "",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to located Bash in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_ = os.Setenv("PATH", tt.targetPath)

			e := &executor{
				absExec: &executors.AbstractExecutor{},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_runmechanisms.NewMockRunner(ctrl)
	m.EXPECT().JobScriptOutput(gomock.Eq("error.bash")).Return(errors.New("error message"))
	m.EXPECT().JobScriptOutput(gomock.Eq("good.bash")).Return(nil)

	tests := map[string]shellTests{
		"error encountered executing job script": {
			scriptPath: "error.bash",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"script executor successfully": {
			scriptPath: "good.bash",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Runner:     m,
					ScriptPath: tt.scriptPath,
				},
			}
			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestExecutor_Cleanup(t *testing.T) {
	e := NewExecutor(&executors.AbstractExecutor{})
	err := e.Cleanup()
	assert.Nil(t, err, "Cleanup successfully skipped")
}
