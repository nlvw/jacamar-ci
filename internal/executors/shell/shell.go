package shell

import (
	"errors"
	"os/exec"

	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
)

type executor struct {
	absExec *executors.AbstractExecutor
}

func (e *executor) Prepare() error {
	_, err := exec.LookPath("bash")
	if err != nil {
		return errors.New("unable to located Bash in the CI environment")
	}

	return nil
}

func (e *executor) Run() error {
	return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
}

func (e *executor) Cleanup() error {
	// No shell specific cleanup required.
	return nil
}

// NewExecutor generates a valid Shell executor that fulfills the executors.Executor interface.
func NewExecutor(ae *executors.AbstractExecutor) *executor {
	return &executor{
		absExec: ae,
	}
}
