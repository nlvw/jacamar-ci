package cobalt

import (
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type cobaltTests struct {
	files []string
	log   string

	mng *mock_batch.MockManager // batch
	run *mock_runmechanisms.MockRunner
	msg *mock_logging.MockMessenger

	prepare func(t *testing.T, s string)

	assertError func(t *testing.T, err error)
	assertSlice func(t *testing.T, s []string)
}

func Test_executor_Prepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	miss := mock_configure.NewMockConfigurer(ctrl)
	miss.EXPECT().Batch().Return(configure.Batch{})

	path, _ := filepath.Abs("../../../test/scripts/schedulers")
	found := mock_configure.NewMockConfigurer(ctrl)
	found.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: path,
	})

	tests := map[string]struct {
		cfg         configure.Configurer
		assertError func(*testing.T, error)
	}{
		"qsub identified": {
			cfg: found,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"qsub not found": {
			cfg: miss,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to locate Cobalt (qsub) in the CI environment")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &executors.AbstractExecutor{
					Cfg:   tt.cfg,
					Stage: "prepare_exec",
				},
			}
			err := e.Prepare()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_Cleanup(t *testing.T) {
	e := NewExecutor(&executors.AbstractExecutor{})
	err := e.Cleanup()
	assert.Nil(t, err, "Cleanup successfully skipped")
}
