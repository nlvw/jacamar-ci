package cobalt

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/logging"
)

// createFiles takes a list of files (prefer full path) and generates
// and empty file. Errors are printed as warnings.
func createFiles(files []string, msg logging.Messenger) {
	for _, file := range files {
		fp, _ := filepath.Abs(file)
		/* #nosec */
		// variable file path required
		_, err := os.OpenFile(fp, os.O_CREATE, 0600)
		if err != nil {
			msg.Warn("Error while attempting to create file (%s): %s", file, err.Error())
		}
	}
}

// qsubJobID parses the output from a successful qsub command in order
// to identify the jobID. Verifies jobID by confirming integer conversion.
func qsubJobID(out string) (string, error) {
	out = strings.TrimSpace(out)
	lines := strings.Split(out, "\n")
	id := lines[len(lines)-1]
	if _, err := strconv.Atoi(id); err != nil {
		return "", err
	}
	return id, nil
}

// exitStatus checks the (jobID).cobaltlog file for a
// string associated with a successful job.
func exitStatus(log string) (es string, err error) {
	file, err := os.Open(filepath.Clean(log))
	if err != nil {
		return "", fmt.Errorf("unable to open CobaltLog: %w", err)
	}

	/* #nosec */
	// file is not written too
	defer file.Close()

	sc := bufio.NewScanner(file)
	for sc.Scan() {
		line := sc.Text()
		if strings.Contains(line, "task completed normally with an exit code of 0") {
			return "0", nil
		}
	}
	if err := sc.Err(); err != nil {
		return "", fmt.Errorf("scanner error: %w", err)
	}

	return "1", errors.New("unable to find successful exit code in CobaltLog")
}

func summarizeErrors(log, id string, msg logging.Messenger) {
	key := strings.TrimSpace(strings.ToLower(os.Getenv(summaryVar)))

	if key == "true" || key == "1" {
		log, _ = filepath.Abs(log)
		/* #nosec */
		// variable file path required
		data, err := ioutil.ReadFile(log)
		if err != nil {
			msg.Warn(strings.Join([]string{"Unable to read", log, "for error summary", err.Error()}, " "))
		}

		if len(data) != 0 {
			msg.Notify(strings.Join([]string{"Cobalt job (", id, ") error summary:"}, " "))
			msg.Stderr(string(data))
		}
	}
}
