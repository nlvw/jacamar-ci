package logging

import (
	"io/ioutil"
	"runtime"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
)

func Test_Messengers(t *testing.T) {
	msg := NewMessenger()

	// We currently aren't testing the output, just verify there are
	// no unexpected panics or visually confirm during development.
	t.Run("Testing printing functions", func(t *testing.T) {
		msg.Stdout("hello stdout")
		msg.Stderr("hello stderr")
		msg.Stderr("hello %s", "world")
		msg.Warn("hello %s", "warning")
		msg.Notify("hello notification")
		msg.Error("hello %s\n", "error")
	})
}

func TestEstablishLogger(t *testing.T) {
	if runtime.GOOS != "linux" {
		t.Skip("tests only for Linux")
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	def := mock_configure.NewMockConfigurer(ctrl)
	def.EXPECT().Auth().Return(configure.Auth{
		Logging: configure.Logging{
			Enabled: true,
		},
	})
	def.EXPECT().General().Return(configure.General{
		Name: "test",
	})

	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().Auth().Return(configure.Auth{})

	type args struct {
		stage string
		cfg   configure.Configurer
		req   envparser.RequiredEnv
	}
	tests := map[string]struct {
		args         args
		assertLogrus func(*testing.T, *logrus.Entry)
		assertError  func(*testing.T, error)
	}{
		"default logging enabled but underlying syslog error": {
			args: args{
				stage: "cleanup",
				cfg:   def,
				req: envparser.RequiredEnv{
					JobID:       "123",
					RunnerShort: "short",
				},
			},
			assertError: func(t *testing.T, err error) {
				// targeting golang image test environment, we need to update
				// if additional logging options are added (e.g., log to specific file).
				assert.EqualError(t, err, "Unix syslog delivery error")
			},
		},
		"logging disabled, empty entry returned": {
			args: args{
				cfg: none,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertLogrus: func(t *testing.T, l *logrus.Entry) {
				assert.Equal(t, ioutil.Discard, l.Logger.Out)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := EstablishLogger(tt.args.stage, tt.args.cfg, tt.args.req)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertLogrus != nil {
				tt.assertLogrus(t, got)
			}
		})
	}
}
