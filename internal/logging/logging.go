// Package logging maintains interface for two core supported types of logging
// that occur within Jacamar. First is communication with the job log that is
// handled via a Messenger. Secondly is the optionally configured system
// logging that is provided via the Logger. Any system logging is targeted at
// a privileged user with Jacamar-Auth.
package logging

import (
	"fmt"
	"io/ioutil"
	"log/syslog"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/configure"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envparser"
)

// Using the same codes as upstream GitLab.
const (
	ANSIBoldRed   = "\033[31;1m" // Error
	ANSIBoldGreen = "\033[32;1m" // Notify
	ANSIYellow    = "\033[0;33m" // Warning
	ANSIReset     = "\033[0;m"
)

// Messenger is the interface to conveying key user information to the
// current CI job log via stdout/stderr to the GitLab runner. Messages
// of sufficient lever can be wrapped in appropriate ANSI characters to
// increase visibility to the user.
type Messenger interface {
	// Stdout prints a formatted message (no color) to stdout.
	Stdout(string, ...interface{})
	// Stderr prints a formatted message (no color) to stderr.
	Stderr(string, ...interface{})
	// Warn prints a formatted message (yellow text) to stdout.
	Warn(string, ...interface{})
	// Notify prints a formatted message (bold green text) to stdout.
	Notify(string, ...interface{})
	// Error prints a formatted message (bold red text) to stderr.
	Error(string, ...interface{})
}

type jobLog struct{}

// NewMessenger generates a functional Messenger interface that should be used
// to convey details/messages to the job log.
func NewMessenger() Messenger {
	return jobLog{}
}

// Stdout prints a formatted message (no color) to stdout.
func (j jobLog) Stdout(msg string, a ...interface{}) {
	msg = format(msg, a...)
	printStdout(msg)
}

// Stderr prints a formatted message (no color) to stderr.
func (j jobLog) Stderr(msg string, a ...interface{}) {
	msg = format(msg, a...)
	printStderr(msg)
}

// Warn prints a formatted message (yellow text) to stdout.
func (j jobLog) Warn(msg string, a ...interface{}) {
	msg = ANSIYellow + format(msg, a...) + ANSIReset
	printStdout(msg)
}

// Notify prints a formatted message (bold green text) to stdout.
func (j jobLog) Notify(msg string, a ...interface{}) {
	msg = ANSIBoldGreen + format(msg, a...) + ANSIReset
	printStdout(msg)
}

// Error prints a formatted message (bold red text) to stderr.
func (j jobLog) Error(msg string, a ...interface{}) {
	msg = ANSIBoldRed + format(msg, a...) + ANSIReset
	printStderr(msg)
}

func format(msg string, a ...interface{}) string {
	if a == nil {
		return msg
	}
	return fmt.Sprintf(msg, a...)
}

func printStdout(msg ...interface{}) {
	_, _ = fmt.Fprintln(os.Stdout, msg...)
}

func printStderr(msg ...interface{}) {
	_, _ = fmt.Fprintln(os.Stderr, msg...)
}

// EstablishLogger uses a combination of known/trusted environmental context coupled with
// administrator configuration to establish rules for Logrus.
func EstablishLogger(
	stage string,
	cfg configure.Configurer,
	req envparser.RequiredEnv,
) (*logrus.Entry, error) {
	if !cfg.Auth().Logging.Enabled {
		ll := logrus.New()
		ll.Out = ioutil.Discard
		return logrus.NewEntry(ll), nil
	}

	host, err := os.Hostname()
	if err != nil {
		// Failure to identify hostname should not result in a job failure.
		host = "unknown"
	}

	ll := logrus.WithFields(logrus.Fields{
		"jobID":        req.JobID,
		"runner-short": req.RunnerShort,
		"jacamar-name": cfg.General().Name,
		"ci-stage":     stage,
		"hostname":     host,
	})

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{
		DisableTimestamp: true,
	})

	writer, err := syslog.New(syslog.LOG_DEBUG, "jacamar-auth")
	if err != nil {
		return nil, err
	}
	logrus.SetOutput(writer)

	return ll, nil
}
