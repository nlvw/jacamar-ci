// Package augmenter offers a rule enforced mechanism for updating
// the GitLab generated CI job script to conform to required behaviors.
package augmenter

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"regexp"
	"strings"
)

// CredentialsLoc is the file location (relative to the CI_PROJECT_DIR) where
// credentials are stored for use with GIT_ASKPASS.
const CredentialsLoc = "/.git/credentials/pass"

// Rules defines strict regulations for any actions that will be directed towards a
// provided script. No values are required and default actions are defined in all cases.
type Rules struct {
	// UnrestrictedCmdline allows for unfettered usages of tokens via the command line in
	// previously identified locations (git, artifact-uploader, and artifact-downloader).
	// Enabling this will allow these identified commands to run without restriction and
	// should only be done in control environments or when proc(5) has been mounted
	// to restrict access.
	UnrestrictedCmdline bool
	// AllowUserCredentials indicates if Git will allow credential storage are part of
	// the CI process. This behavior is dictated by the user's global Git configuration
	// and as such may not be present. On shared/static resources enabling this may
	// break concurrently running job.
	AllowUserCredentials bool
	// TrustedHost is the trusted upstream hostname (e.g., gitlab.example.com )used in
	// find-and-replace functionality.
	TrustedHost string
	// TargetHost replaces all instances of TrustedURL. You must provide both Trusted
	// and Target values.
	TargetHost string
	// TrustedJobToken will act as a trust worthy source for the existing CI_JOB_TOKEN to
	// assist in replacing all instance without the need to rely on a static list of
	// variables. At this time it is required to supply a value if you are going to
	// also attempt to introduce a TargetJobToken.
	TrustedJobToken string
	// TargetJobToken replaces all instances of the TrustedJobToken. You must provide both
	// Trusted and Target values.
	TargetJobToken string
	// RedactedEnvVars can be defined with a list of environment variable keys of which
	// whose values will be redacted from the script. All keys are case sensitive.
	RedactedEnvVars  []string
	redactedPrefixes []string
}

const (
	git       = `$\'git\'`
	gitFetch  = `$\'git\' "fetch" "origin"`
	gitRemote = `$\'git\' "remote"`
	gitCIUser = `gitlab-ci-token`
	artifacts = `$\'gitlab-runner\' "artifacts-`
)

// JobScript accepts a filepath  of a CI job script as well as the associated
// sub-stage (https://docs.gitlab.com/runner/executors/custom.html#run) identified
// during run_exec as well as a trust CI_PROJECT_DIR equivalent.
// These details are used in conjunction with the constraints
// established by the rules to ingest the script and restructure it accordingly.
// The finalized script is returned in its completion as an un-encoded string.
func (r Rules) JobScript(filepath, stage, projectDir string) (string, error) {
	contents, err := openScript(filepath)
	if err != nil {
		return "", err
	}

	// At this time we do not support any changes to the prepare_script.
	if stage != "prepare_script" {
		r.redactedPrefixes = r.prepRedacted()
		for i, line := range contents {
			if strings.HasPrefix(line, ": | eval") {
				eval := r.realize(stage, projectDir, strings.Split(line, `\n`))
				contents[i] = strings.Join(eval, `\n`)
			}
		}
	}

	return strings.Join(contents, "\n"), nil
}

func (r Rules) prepRedacted() (redacted []string) {
	for _, s := range r.RedactedEnvVars {
		// Being explicit with the export statement should ensure we only
		// attempt to locate and redact runner generated variables.
		redacted = append(redacted, "export "+s+"=$\\'")
	}
	return redacted
}

func (r Rules) realize(stage, dir string, eval []string) []string {
	switch {
	case !r.UnrestrictedCmdline:
		eval = r.restrictCmdLine(stage, dir, eval)
		fallthrough
	case !r.AllowUserCredentials && stage == "get_sources":
		eval = r.restrictUserCreds(eval)
		fallthrough
	case len(r.redactedPrefixes) != 0:
		eval = r.redactVars(eval)
		fallthrough
	case r.TrustedHost != "" && r.TargetHost != "":
		eval = replaceSlice(eval, urlHost(r.TrustedHost), urlHost(r.TargetHost))
		fallthrough
	case r.TrustedJobToken != "" && r.TargetJobToken != "":
		eval = replaceSlice(eval, r.TrustedJobToken, r.TargetJobToken)
	}

	return eval
}

func urlHost(s string) string {
	u, err := url.Parse(s)
	if err != nil || u.Host == "" {
		return s
	}

	return u.Host
}

// restrictCmdLine realizes the specific implementation of the UnrestrictedCmdline rule.
func (r Rules) restrictCmdLine(stage, dir string, eval []string) []string {
	if potentialGitStage(stage) {
		eval = append(eval, "")
		copy(eval[2:], eval[1:])
		eval[1] = `export GIT_ASKPASS=$\'` + dir + CredentialsLoc + `\'`
	}

	if stage == "get_sources" {
		eval = containsAction(eval, gitRemote, gitRemoveToken)
	}

	if strings.Contains(stage, "artifacts") {
		eval = containsAction(eval, artifacts, artifactsExportToken)
	}

	return eval
}

// potentialGitStage identifies if the stage has the potential for Git command.
func potentialGitStage(stage string) bool {
	if stage == "get_sources" || stage == "after_script" || stage == "build_script" {
		return true
	}

	// We are currently unclear on the functionality of all potential step_* scripts,
	// as such it is safest to treat them all as we had a build_script.
	return strings.HasPrefix(stage, "step_")
}

func containsAction(eval []string, substr string, action func(string) string) []string {
	for i, val := range eval {
		if strings.Contains(val, substr) {
			eval[i] = action(val)
		}
	}
	return eval
}

func replaceSlice(s []string, old, new string) []string {
	for i := range s {
		s[i] = strings.ReplaceAll(s[i], old, new)
	}
	return s
}

// restrictUserCreds realizes the specific implementation of the AllowUserCredentials rule.
func (r Rules) restrictUserCreds(eval []string) []string {
	index := len(git)
	for i := range eval {
		if strings.HasPrefix(eval[len(eval)-1-i], gitFetch) {
			line := eval[len(eval)-1-i]
			eval[len(eval)-1-i] = line[:index] + ` -c credential.helper= ` + line[index:]
		}
	}
	return eval
}

// redactVars realizes the specific implementation of the RedactedEnvVars rule.
func (r Rules) redactVars(eval []string) []string {
	for _, pre := range r.redactedPrefixes {
		for i, val := range eval {
			if strings.HasPrefix(val, pre) {
				eval[i] = pre + "VALUE_REMOVED\\'"
				// skip once a single redaction has been found ?
			}
		}
	}
	return eval
}

// gitRemoveToken parses any provided git command for a token in the https:// url
// and removes it. Example url where the goal is to remove 'ciJobToken':
// "https://gitlab-ci-token:ciJobToken@gitlab.example.com/user/scratch-space.git"
func gitRemoveToken(s string) string {
	i := strings.Index(s, gitCIUser)
	j := strings.Index(s[i:], "@")
	if i > -1 && j > -1 {
		pre := s[:i+len(gitCIUser)]
		post := (s[i:])[j:]
		s = pre + post
	}
	return s
}

func openScript(filepath string) (s []string, err error) {
	/* #nosec */
	// variable file path required
	b, err := ioutil.ReadFile(filepath)
	if err != nil {
		return
	}
	s = strings.Split(string(b), "\n")
	return
}

// artifactsExportToken removes the use of --id to provide a token to the
// artifact uploader/downloader and instead relies on export (Bash).
func artifactsExportToken(s string) string {
	// $\'gitlab-runner\' "artifacts-uploader" "--url" "https://gitlab.example.com/"
	// "--token" "ciJobToken" "--id" "12"
	re := regexp.MustCompile(`"--token"\s"(.*?)"`)
	match := re.FindAllStringSubmatch(s, -1)
	for _, v := range match {
		var sb strings.Builder
		sb.WriteString(`export CI_JOB_TOKEN=$\'`)
		sb.WriteString(v[1])
		sb.WriteString(`\' && `)
		sb.WriteString(strings.ReplaceAll(s, v[0], ""))
		s = sb.String()
	}

	return s
}

// CreateGitAskpass safely generates a script for handling Git credentials
// (see https://git-scm.com/docs/gitcredentials) that will provide the token
// back to any git command so long as the GIT_ASKPASS environment variable
// is set. Invoking this function is required to realize the AllowUserCredentials
// rule but remain distinct to support distinct workflows that come with
// executing CI on a behalf of a user. The expected directory should equate
// to the CI_PROJECT_DIR and contain the .git/ folder.
func CreateGitAskpass(dir, token string) error {
	if strings.HasSuffix(dir, "/") {
		dir = dir[:len(dir)-1]
	}

	dir += "/.git"
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err := os.Mkdir(dir, 0700); err != nil {
			return err
		}
	}

	dir += "/credentials"
	if _, err := os.Stat(dir); !os.IsNotExist(err) {
		if err := os.RemoveAll(dir); err != nil {
			return err
		}
	}
	if err := os.Mkdir(dir, 0700); err != nil {
		return err
	}

	dir += "/pass"
	/* #nosec */
	// creating script, 700 permissions required
	file, err := os.OpenFile(dir, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0700)
	if err != nil {
		return err
	}

	_, err = file.WriteString(fmt.Sprintf("#!/usr/bin/env bash\n\nbuiltin echo %s", token))
	if err != nil {
		_ = file.Close()
		return err
	}

	if err := file.Close(); err != nil {
		return err
	}

	return nil
}

// GitAskpassDir returns the directory containing the script based upon
// the supplied CI_PROJECT_DIR.
func GitAskpassDir(dir string) string {
	if strings.HasSuffix(dir, "/") {
		return dir + ".git/credentials"
	}
	return dir + "/.git/credentials"
}
