module gitlab.com/ecp-ci/jacamar-ci

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/alexflint/go-arg v1.3.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang/mock v1.4.3
	github.com/google/go-cmp v0.5.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/radovskyb/watcher v1.0.7
	github.com/sirupsen/logrus v1.7.0
	github.com/square/go-jose v2.5.1+incompatible
	github.com/stretchr/testify v1.6.1
	gitlab.com/ecp-ci/jacamar-plugins v0.0.0-20210104225958-5a7d25cd2c28
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
