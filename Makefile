SHELL := bash
.ONESHELL:
.DELETE_ON_ERROR:
.DEFAULT_GOAL := build
MAKEFLAGS += --no-builtin-rules

# Project & structure variables.
PKG = gitlab.com/ecp-ci/jacamar-ci
INTERNAL_PKG = ${PKG}/internal
COVER_REPORT := cover.out
BUILDDIR ?= binaries/
ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif
INSTALL_PREFIX := $(if $(DESTDIR),$(DESTDIR)/$(PREFIX),$(PREFIX))

# Version variables.
GITCOMMIT := $(shell ./test/scripts/gitcommit.bash)
GITBRANCH := $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
GOVERSION := $(shell go version | cut -c12- | awk '{ gsub (" ", "-", $$0); print}')
BUILDDATE := $(shell date -u +"%Y-%m-%dT%T%z")
ifndef GITCOMMIT
VERSION ?= $(shell cat VERSION)
else
VERSION ?= $(shell cat VERSION).pre.${GITCOMMIT}
endif

# Go variables.
GOCMD := go
LDFLAGS = -X ${INTERNAL_PKG}/version.version=${VERSION} \
		  -X ${INTERNAL_PKG}/version.gitCommit=${GITCOMMIT} \
		  -X ${INTERNAL_PKG}/version.gitBranch=${GITBRANCH} \
		  -X ${INTERNAL_PKG}/version.goVersion=${GOVERSION} \
		  -X ${INTERNAL_PKG}/version.buildDate=${BUILDDATE} \
		  -s \
		  -w

.PHONY: all
all:
	$(MAKE) build
	$(MAKE) install

###################
# Build & Install #
###################

.PHONY: build
build:
	mkdir -p ${BUILDDIR}
	CGO_ENABLED=1 ${GOCMD} build -trimpath -ldflags "${LDFLAGS}" -o ${BUILDDIR}jacamar cmd/jacamar/main.go
	CGO_ENABLED=1 ${GOCMD} build -trimpath -ldflags "${LDFLAGS}" -o ${BUILDDIR}jacamar-auth cmd/jacamar-auth/main.go

.PHONY: install
install:
	install -d $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar-auth $(INSTALL_PREFIX)/bin

#############
# Packaging #
#############

.PHONY: rpm-ci
rpm-ci:
	VERSION=${VERSION} bash ./test/scripts/package/rpm.bash ci

.PHONY: release
release:
	$(MAKE) clean
	VERSION=${VERSION} bash ./test/scripts/package/rpm.bash release

.PHONY: runner-rpm-ci
runner-rpm-ci:
	bash ./test/scripts/package/runner-rpm.bash ci

.PHONY: runner-release
runner-release:
	$(MAKE) clean
	bash ./test/scripts/package/runner-rpm.bash release

###########
# Testing #
###########

.PHONY: test 
test:
	${GOCMD} test -p 1 -coverprofile ${COVER_REPORT} -cover -timeout 2m -v ./... \
		&& ${GOCMD} tool cover -func=${COVER_REPORT}

.PHONY: test-dind
test-dind:
	@bash ./test/scripts/test_dind.bash test

.PHONY: test-quality
test-quality: 
	# https://github.com/golangci/golangci-lint
	golangci-lint --version
	golangci-lint run

.PHONY: test-security
test-security:
	# https://github.com/securego/gosec
	gosec --version
	gosec -exclude-dir=tools -exclude-dir=test  -exclude-dir=build ./...

.PHONY: coverage
coverage:
	${GOCMD} tool cover -func=${COVER_REPORT}

####################
# Pavilion Testing #
####################

.PHONY: pav-docker
pav-docker:
	@bash ./test/scripts/pav/docker.bash test

.PHONY: pav-docker-slurm
pav-docker-slurm:
	@bash ./test/scripts/pav/slurm.bash test

.PHONY: pav-docker-clean
pav-docker-clean:
	@bash ./test/scripts/pav/docker.bash clean

# ALCF Testing
.PHONY: pav-theta
pav-theta:
	@bash ./test/scripts/pav/theta.bash test

.PHONY: pav-theta-clean
pav-alcf-clean:
	@bash ./test/scripts/pav/theta.bash clean

# OLCF Testing
.PHONY: pav-ascent
pav-ascent:
	@bash ./test/scripts/pav/ascent.bash test

.PHONY: pav-ascent-clean
pav-olcf-clean:
	@bash ./test/scripts/pav/ascent.bash clean

#########
# Misc. #
#########

.PHONY: clean
clean:
	${GOCMD} clean
	rm -rf $(BUILDDIR) rpmbuild/ rpms/
	rm -f ${COVER_REPORT}

.PHONY: version
version:
	@echo Version: ${VERSION}
	@echo Git Commit: ${GITCOMMIT}
	@echo Git Branch: ${GITBRANCH}
	@echo Go Version: ${GOVERSION}
	@echo Built: ${BUILDDATE}

# Rebuild all mocks (https://github.com/golang/mock)
.PHONY: mocks
mocks:
	bash ./test/scripts/mocks.bash
